import React,{Component} from 'react'
import Aux from '../hoc/Auxhoc'
import Burger from '../components/Burger/Burger'
import BuilderControl  from '../components/Build_Controls/BuildControl';

const INGREDIENT_PRICE = {
	'meat' : 1.2,
	'salad' : 0.3,
	'bacon' : 0.7,
	'cheese' : 0.6
}

class BurgerBuilder extends React.Component {
	
	constructor(props) {
		super(props)
		this.state = {
			ingredient :{
				meat : 0,
				cheese : 0,
				bacon : 0,
				salad : 0,
			},
			totalprice : 4,
		}
	};


    addItemHandler =(key) => {
    	//console.log('[BurgerBuilder.js] {addItemHandler} called...');

    	let oldVal = this.state.ingredient[key];
    	let updatedVal = oldVal + 1;
    	let ingred = Object.assign({}, this.state.ingredient);
    	ingred[key] = updatedVal;
		
		let newPrice = this.state.totalprice + INGREDIENT_PRICE[key];


		console.log('[BurgerBuilder.js] {addItemHandler} state : ',ingred);
		this.setState(
			{ingredient : ingred, totalprice : newPrice}
		);
	};


    removeItemHandler =(key) => {

    	//console.log('[BurgerBuilder.js] {addItemHandler} called...');

    	let oldVal = this.state.ingredient[key];
    	if(oldVal <= 0){
    		return;
    	}

    	let updatedVal = oldVal - 1;
    	let ingred = Object.assign({}, this.state.ingredient);
    	ingred[key] = updatedVal;
		
		let newPrice = this.state.totalprice - INGREDIENT_PRICE[key];


		console.log('[BurgerBuilder.js] {addItemHandler} state : ',ingred);
		this.setState(
			{ingredient : ingred, totalprice : newPrice}
		);

	};


	componentWillUpdate(){
		console.log('[BurgerBuilder.js] {componentWillUpdate()} called');
		console.log('[BurgerBuilder.js] {componentWillUpdate()} state : ',this.state);
	}

	componentDidUpdate(){
		console.log('[BurgerBuilder.js] {componentDidUpdate()} called');
		console.log('[BurgerBuilder.js] state : ',this.state);
	}; 

	render(){
		let buildercontrol = Object.keys(this.state.ingredient);

		return <Aux> 
					<Burger ingredient={this.state.ingredient} />

					{buildercontrol.map((key)=>{
						return <BuilderControl type={key} addItem={()=>{this.addItemHandler(key)}} removeItem={()=>{this.removeItemHandler(key)}} />
					})
					}	

				</Aux>
	}

}

export default BurgerBuilder;