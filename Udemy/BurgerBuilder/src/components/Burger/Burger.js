import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const burger = (props)=>{
	
	
	//console.log('array : ',arr);
	let burger = null;

	let arr = Object.keys(props.ingredient)
	.map((key)=>{
		console.log('[Buger.js] props.ingredient : ',props.ingredient[key]);
		return [...Array(props.ingredient[key])].map((_,i)=>{
			return <BurgerIngredient key={key+i} type={key}/>
		});
	})
	.reduce((arr,elem)=>{ return arr.concat(elem) },[]);
	console.log('arr : ',arr);
			
	if(arr.length == 0){
		arr =  <p>'Please add Items to your Burger'</p>;
	}	

	return <div>
				<BurgerIngredient type='breadtop' />
				{arr}
				<BurgerIngredient type='breadbottom' />
			</div>
	
}

export default burger;