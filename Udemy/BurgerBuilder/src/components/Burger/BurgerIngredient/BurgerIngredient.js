import React,{Component} from 'react';
import classes from './BurgerIngredient.css';
import style from './Testcss.css';
import PropTypes from 'prop-types';


class Burgeringredient extends Component{

	render(){
		let ingredient = null;

		switch (this.props.type) {

			case ('breadtop'):
				ingredient = <div className={classes.BreadTop}> .</div>;
				break;

			case ("breadbottom"):
				ingredient = <div className={classes.BreadBottom}>.</div>;
				break;

			case ("meat"):
				ingredient = <div className={classes.Meat}>.</div>;
				break;

			case ("cheese"):
				ingredient = <div className={classes.Cheese}>.</div>;
				break;

			case ("salad"):
				ingredient = <div className={classes.Salad} >.</div>;
				break;

			case ("bacon"):
				ingredient = <div className={classes.Bacon}>.</div>;
				break;

		}
			//console.log('[BurgerIngredient.js] called')
		return ingredient;

	}
}

Burgeringredient.propTypes = {
	type : PropTypes.string.isRequired
};

export default Burgeringredient;