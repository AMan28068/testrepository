package dynamic;

public class m {

	public static int MaxSub(int ar[],int x)
	{
		int len=ar.length;
		int totlen=1;
		int start =0;
		int end=1;
		int sum=ar[0];
		int i=0;
		int min=1000;
		boolean t=false;
		while(len>=end) // == because to check the last element
		{
			if(sum<=x && len==end) // break when sum<x and len== end 
			{
				break;
			}
			if(sum<=x)
			{
				sum+=ar[end];
				end++;
				totlen++;
				t=false; //to reduce len the second time when sum>x 
			}
			else if(sum>x)
			{
				if(start==end)
				{
					totlen=1;
				}
				if(start!=end)
				{
					sum-=ar[start];
					start++;
					if(t)
					{
						totlen--;
					}
					t=true;	//change to true for second time sum>x
				}
				if(totlen<min)
				{
					min=totlen;
				}
				
			}
			 
			
		}
		if(start==0)
		{
			System.out.println("NP");
		}
		else
		{
			System.out.println(min);
		}
		return 0;
		
	}
	public static void main(String arp[])
	{
		int arr[]= {1, 4, 45, 6, 0, 19};
		MaxSub(arr, 51);
	}
}
