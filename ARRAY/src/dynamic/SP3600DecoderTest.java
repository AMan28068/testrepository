package dynamic;



import java.awt.HeadlessException;
import java.math.BigInteger;
import java.nio.IntBuffer;
import java.util.Date;
import java.util.Hashtable;
import dynamic.SP3600Bean;
public class SP3600DecoderTest {
    
	public static void main(String ar[])
    {
		SP3600Bean oSPBean=new SP3600Bean();
        String sDevice="*GS06, 351535053999223,235833280213,9C,SYS:G6S;V1.01;V1.01,GPS:A;8;N23.164351;E113.428515;0;0;37;0.85;0.35,GSM:5;4;460;0;2731;BB41;-82;460;0;2731;436E;-81;460;0;2731;436D;-94,COT:4294967295;99999-00-00;0F1000;1P4294967295;2P1234;3F1000,ADC:12.60;3.99;10.00;10.00,DTT: ABCF;BC;FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;FFFFFFFFF;FFFFFFFFFFFFFFFF;FF,IWD:0;0;000133B29;1;1;3400012038C21;100,ETD:11;30#";
        String sOldIOValues="1=0,2=0,3=0,4=0,5=0,6=0,7=0,-9=0,14=0,15=0,16=0,17=0,18=0,19=0,20=0,21=0,22=0,23=0,25=0,26=0,27=0,28=0,29=0,30=0,31=0,32=0,33=0,34=0,35=0,36=0,37=0,38=0,39=0,40=0,41=0,42=0,43=0,44=0,45=0,46=0,47=0,48=0,49=0,50=0,51=0,52=0,53=0,54=0,55=0,255=0,253=0,257=0,258=0"
                                    +",56=0,57=0,58=0,59=0,60=0,61=0,987=0,988=0,78=0"
                                    +",260=0,261=0,24=0,240=0,985=0,997=0,998=0";
        Hashtable<Integer,Integer> hOldIOvalues=new Hashtable<Integer,Integer>();
        String sDeviceData[]=sDevice.split(",");
        String sGSMDeviceData[]=null;
        String sGPSDeviceData[]=null;
        String sCOTDeviceData[]=null;
        String sADCDeviceData[]=null;
        String sIbutton[]=null;
        String sDeviceStatusData[]=null;
        String sTemp="";
                try
        {
            oSPBean.setlImei(Long.parseLong(sDeviceData[1].trim()));
            oSPBean.setsDate(sDeviceData[2].substring(10,12)+sDeviceData[2].substring(8,10)+sDeviceData[2].substring(6,8));
            oSPBean.setsTime(sDeviceData[2].substring(0,6));
            oSPBean.setlFixtime(_getUTCSeconds_YMD_HMS(oSPBean.getsDate(),oSPBean.getsTime()));
            String sEventBin=sDeviceData.equals("")?"":HexToBinary(sDeviceData[3]);
            oSPBean.setiEvent(sDeviceData[3].equals("")?-1:Integer.parseInt(sEventBin.substring(1,sEventBin.length()),2));
            String aTemp[]=sOldIOValues.split(",");
            for(int i=0;i<aTemp.length;)
            {
                hOldIOvalues.put(Integer.parseInt(aTemp[i].split("=")[0]),Integer.parseInt(aTemp[i].split("=")[1]));


                i=i+1;
            }
            System.out.println(hOldIOvalues);
            for(int i=4;i<sDeviceData.length;i++) {
                oSPBean.setsCommand(sDeviceData[i].substring(0,3));
                //System.out.println(sCommand);
                if(oSPBean.getsCommand().equals("GPS")) {
                    sGPSDeviceData = sDeviceData[5].split(";");
                    oSPBean.setbGPSValidity(sGPSDeviceData[0].split(":")[1].equals("A") ? true : false);
                    oSPBean.setiSatelites(Integer.parseInt(sGPSDeviceData[1]));
                    oSPBean.setdLatitude(Double.parseDouble(sGPSDeviceData[2].substring(1, sGPSDeviceData[2].length())));
                    oSPBean.setdLongitude(Double.parseDouble(sGPSDeviceData[3].substring(1, sGPSDeviceData[3].length())));
                    oSPBean.setiAzimuth(Integer.parseInt(sGPSDeviceData[5]));

                    if (sGPSDeviceData[2].charAt(0) == 'S') {
                    	if(oSPBean.getdLatitude()>0)
                    		oSPBean.setdLatitude(-oSPBean.getdLatitude());
                    }
                    if (sGPSDeviceData[3].charAt(0) == 'W') {
                        if(oSPBean.getdLongitude()>0)
                        	oSPBean.setdLongitude(-oSPBean.getdLongitude());
                    }
                    oSPBean.setIspeed(Integer.parseInt(sGPSDeviceData[4]));
                    oSPBean.setiAltitude(Integer.parseInt(sGPSDeviceData[6]));
                    oSPBean.setdHDOP(Double.parseDouble(sGPSDeviceData[7]));
                    oSPBean.setdVDOP(Double.parseDouble(sGPSDeviceData[8]));
                }
                else if(oSPBean.getsCommand().equals("GSM")) {
                    sGSMDeviceData = sDeviceData[6].split(";");
                    oSPBean.setiGSM(Integer.parseInt(sGSMDeviceData[1]));
                    System.out.println("iGSM::::"+oSPBean.getiGSM());
                    if(sGSMDeviceData.length>0) {
                        oSPBean.setiMCC1(Integer.parseInt(sGSMDeviceData[2]));
                        oSPBean.setiMNC1(Integer.parseInt(sGSMDeviceData[3]));
                        oSPBean.setiLAC1(Integer.parseInt(sGSMDeviceData[4], 16));
                        oSPBean.setiCID1(Integer.parseInt(sGSMDeviceData[5], 16));
                        oSPBean.setiRSS1(Integer.parseInt(sGSMDeviceData[6]));
                    }
                    else if(sGSMDeviceData.length>8) {
                        oSPBean.setiMCC2(Integer.parseInt(sGSMDeviceData[7]));
                        oSPBean.setiMNC2(Integer.parseInt(sGSMDeviceData[8]));
                        oSPBean.setiLAC2(Integer.parseInt(sGSMDeviceData[9], 16));
                        oSPBean.setiCID2(Integer.parseInt(sGSMDeviceData[10], 16));
                        oSPBean.setiRSS2(Integer.parseInt(sGSMDeviceData[11]));
                    }
                    else if(sGSMDeviceData.length>13) {
                    	oSPBean.setiMCC3(Integer.parseInt(sGSMDeviceData[12]));
                        oSPBean.setiMNC3(Integer.parseInt(sGSMDeviceData[13]));
                        oSPBean.setiLAC3(Integer.parseInt(sGSMDeviceData[14], 16));
                        oSPBean.setiCID3(Integer.parseInt(sGSMDeviceData[15], 16));
                        oSPBean.setiRSS3(Integer.parseInt(sGSMDeviceData[16]));
                    }
                }
                else if(oSPBean.getsCommand().equals("COT")) {
                    sCOTDeviceData = sDeviceData[7].split(";");
                    oSPBean.setlOdom(Long.parseLong(sCOTDeviceData[0].split(":")[1]));
                    oSPBean.setlIN1(Long.parseLong(sCOTDeviceData[2].substring(2,sCOTDeviceData[2].length())));
                    oSPBean.setlIN2(Long.parseLong(sCOTDeviceData[3].substring(2,sCOTDeviceData[3].length())));
                    oSPBean.setlIN3(Long.parseLong(sCOTDeviceData[4].substring(2,sCOTDeviceData[4].length())));
                    oSPBean.setlIN4(Long.parseLong(sCOTDeviceData[5].substring(2,sCOTDeviceData[5].length())));
                    System.out.println("Lin1 "+oSPBean.getlIN1()+" Lin2 "+oSPBean.getlIN2()+" Lin3 "+oSPBean.getlIN3()+" Lin4 "+oSPBean.getlIN4());
                }
                else if(oSPBean.getsCommand().equals("ADC")) {
                    sADCDeviceData = sDeviceData[8].split(";");
                    oSPBean.setdExtVol(Double.parseDouble(sADCDeviceData[0].split(":")[1]) * 1000);
                    oSPBean.setdBackVol(Double.parseDouble(sADCDeviceData[1]) * 1000);
                    oSPBean.setdADC1Vol(Double.parseDouble(sADCDeviceData[2]) * 1000);
                    oSPBean.setdADC2Vol(Double.parseDouble(sADCDeviceData[3]) * 1000);
                }
                else if(oSPBean.getsCommand().equals("DTT")) {
                    sDeviceStatusData = sDeviceData[9].split(";");
                    sTemp = HexToBinary(sDeviceStatusData[0].split(":")[1]);
                    System.out.println("Binary" + sTemp);
                    oSPBean.setShWorkStatus((short) (sTemp.charAt(0) == '1' ? 1 : 0));
                    oSPBean.setShParked((short) (sTemp.charAt(1) == '1' ? 0 : 1));
                    oSPBean.setShADC1Status((short) (sTemp.charAt(2) == '1' ? 1 : 0));
                    oSPBean.setShADC2Status( (short) (sTemp.charAt(3) == '1' ? 1 : 0));
                    oSPBean.setShGeoFence((short) (sTemp.charAt(4) == '1' ? 1 : 0));
                    oSPBean.setShAccident((short) (sTemp.charAt(5) == '1' ? 1 : 0));
                    //oSPBean.setShHarshBehavior((short) (sTemp.charAt(6) == '1' ? 1 : 0));
                    oSPBean.setShInterRoaming((short) (sTemp.charAt(7) == '1' ? 1 : 0));
                    oSPBean.setShDomesticRoaming((short) (sTemp.charAt(8) == '1' ? 1 : 0));
                    oSPBean.setShAntiJam((short) (sTemp.charAt(9) == '1' ? 1 : 0));
                    oSPBean.setShTow((short) (sTemp.charAt(10) == '1' ? 1 : 0));
                    oSPBean.setShIdle((short) (sTemp.charAt(11) == '1' ? 1 : 0));
                    oSPBean.setShOverSpeed((short) (sTemp.charAt(12) == '1' ? 1 : 0));
                    oSPBean.setShMoving((short) (sTemp.charAt(13) == '1' ? 1 : 0));
                    oSPBean.setShBackupPwrsupp((short) (sTemp.charAt(14) == '1' ? 1 : 0));
                    oSPBean.setShExtPwrsupp((short) (sTemp.charAt(15) == '1' ? 1 : 0));
                    String sTemp1 = HexToBinary(sDeviceStatusData[1]);
                    oSPBean.setShAccOn((short) (sTemp1.charAt(0) == '1' ? 1 : 0));
                    oSPBean.setShDI1((short) (sTemp1.charAt(1) == '1' ? 0 : 1));
                    oSPBean.setShDI2((short) (sTemp1.charAt(2) == '1' ? 0 : 1));
                    oSPBean.setShDI3((short) (sTemp1.charAt(3) == '1' ? 1 : 0));
                    oSPBean.setShDI4((short) (sTemp1.charAt(4) == '1' ? 1 : 0));
                    oSPBean.setShDO1((short) (sTemp1.charAt(5) == '1' ? 1 : 0));
                    oSPBean.setShDO2((short) (sTemp1.charAt(6) == '1' ? 1 : 0));
                    oSPBean.setShD03((short) (sTemp1.charAt(7) == '1' ? 1 : 0));
                }
                else if(oSPBean.getsCommand().equals("IWD")) {

                    sIbutton = sDeviceData[10].split(";");
                    if (sIbutton.length > 5) {
                        System.out.println("Insider");
                        if (sIbutton[1].equals("0"))
                        {
                            oSPBean.setsRfid(sIbutton[2]);
                            System.out.println("RFID "+oSPBean.getsRfid());
                        }
                        if (sIbutton[1].equals("1")) {
                            oSPBean.setdTemp(Double.parseDouble(sIbutton[3])/10.0d);
                        } else if (sIbutton[4].equals("1")) {
                            oSPBean.setdTemp(Double.parseDouble(sIbutton[6])/10.0d);
                        }
                    }
                }
                else if(oSPBean.getsCommand().equals("ETD")) {
                    String sETD[]=sDeviceData[11].split(";");
                    if (oSPBean.getiEvent() != -1) {
                        switch (oSPBean.getiEvent()){
                            case 0:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1)) 	//IOvalue 1
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShTow((short)1);
                                }
                                break;
                            case 1:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 2
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShIdle((short)1);
                                }
                                break;
                            case 2:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 3
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShParked((short)1);
                                }
                                break;
                            case 3:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 4
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShOverSpeed((short)1);
                                    oSPBean.setIspeed(Integer.parseInt(sETD[0].split(":")[1]));
                                }
                                break;
                            case 4:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 5
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShAntiJam((short )1);

                                }
                                break;
                            case 5:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 6
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShGeoFence((short)1);

                                }
                                break;
                            case 6:
                                if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 7
                                {
                                    if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                        oSPBean.setShFirstTimeReport((short)1);

                                }
                                break;
                            case 7:
                                String sTempStr=HexToBinary(sETD[0].split(":")[1]);
                                oSPBean.setShSocket1((short)(sTempStr.charAt(7)=='1'?0:1));
                                oSPBean.setShSocket2((short)(sTempStr.charAt(6)=='1'?0:1));
                                oSPBean.setShSocket3((short)(sTempStr.charAt(5)=='1'?0:1));
                                oSPBean.setShSocket4((short)(sTempStr.charAt(4)=='1'?0:1));
                                oSPBean.setShSocket5((short)(sTempStr.charAt(3)=='1'?0:1));
                                oSPBean.setShMotionSensor((short)(sTempStr.charAt(2)=='1'?0:1));
                                oSPBean.setShFlashStorage((short)(sTempStr.charAt(1)=='1'?0:1));
                                oSPBean.setShVibrationSensor((short)(sTempStr.charAt(0)=='1'?0:1));
                                break;
                            case 8:						
                                if(hOldIOvalues.containsKey((int)253))		//IOvalue 253
                                {
                                    if(hOldIOvalues.get(253)==0) {
                                        oSPBean.setShHarshBrake((short)1);
                                        oSPBean.setShHarshBehavior((short)2);
                                    }
                                    oSPBean.setdXAxis(Double.parseDouble(sETD[0].split(":")[1])/1000.0d);
                                    oSPBean.setdYAxis(Double.parseDouble(sETD[1])/1000.0d);

                                }
                                break;
                            case 9:
                                if(hOldIOvalues.containsKey((int)253))			//IOvalue 253
                                {
                                    if(hOldIOvalues.get(253)==0) {
                                        oSPBean.setShHarshBehavior((short)1);
                                        oSPBean.setShHarshAccceleration((short) 1);
                                    }
                                    oSPBean.setdXAxis(Double.parseDouble(sETD[0].split(":")[1])/1000.0d);
                                    oSPBean.setdYAxis(Double.parseDouble(sETD[1])/1000.0d);

                                }
                                break;
                            case 10:
                                if(hOldIOvalues.containsKey((int)253))			//IOvalue 253
                                {
                                    if(hOldIOvalues.get(253)==0) {
                                        oSPBean.setShHarshBehavior((short)3);
                                        oSPBean.setShHarshCornering((short)1);
                                    }
                                    oSPBean.setdXAxis(Double.parseDouble(sETD[0].split(":")[1])/1000.0d);
                                    oSPBean.setdYAxis(Double.parseDouble(sETD[1])/1000.0d);

                                }
                                break;
                            case 11:
                                if(hOldIOvalues.containsKey((int)255))			//IOvalue 255
                                {
                                    if(hOldIOvalues.get(255)==0) {
                                        oSPBean.setShAccident((short) 1);
                                        oSPBean.setShFrontCollision((short)1);

                                    }
                                    oSPBean.setdXAxis(Double.parseDouble(sETD[0].split(":")[1])/1000.0d);
                                    oSPBean.setdYAxis(Double.parseDouble(sETD[1])/1000.0d);

                                }
                                break;
                            case 12:
                                if(hOldIOvalues.containsKey((int)255))			//IOvalue 255
                                {
                                    if(hOldIOvalues.get(255)==0) {
                                        oSPBean.setShAccident((short)1);
                                        oSPBean.setShRearCollision((short)1);
                                    }
                                    oSPBean.setdXAxis(Double.parseDouble(sETD[0].split(":")[1])/1000.0d);
                                    oSPBean.setdYAxis(Double.parseDouble(sETD[1])/1000.0d);

                                }
                                break;
                            case 13:
                                if(hOldIOvalues.containsKey((int)255))			//IOvalue 255
                                {
                                    if(hOldIOvalues.get(255)==0) {
                                        oSPBean.setShAccident((short)1);
                                        oSPBean.setShTurnOver((short)1);

                                    }
                                    oSPBean.setdXAxis(Double.parseDouble(sETD[0].split(":")[1])/1000.0d);
                                    oSPBean.setdYAxis(Double.parseDouble(sETD[1])/1000.0d);

                                }
                                break;
                            case 14:
                                if(sETD[1].equals("0")) {
                                    if (hOldIOvalues.containsKey((int) oSPBean.getiEvent())) {		//IOvalue 14

                                        if (hOldIOvalues.get(oSPBean.getiEvent()) == 0)
                                            oSPBean.setShIButton((short)1);
                                        oSPBean.setsRfid(sETD[2]);
                                    }
                                }
                                    else if(sETD[1].equals("1"))
                                    {
                                        if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {		//IOvalue 15
                                            if (hOldIOvalues.get(oSPBean.getiEvent() + 1) == 0)
                                                oSPBean.setShTemperature((short)1);
                                            if(sETD.length>3)
                                            {
                                            	oSPBean.setdTemp(Double.parseDouble(sETD[3])/10.0d);
                                            }
                                        }
                                    }
                                break;
                            case 15:
                                if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 16
                                    if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                        oSPBean.setShBalanceLow((short)1);
                                }
                                break;
                            case 16:
                                if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 17
                                    if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                        oSPBean.setShExtPwrsupp((short)1) ;
                                    oSPBean.setdExtVol(Double.parseDouble(sETD[0].split(":")[1])*1000);
                                }
                                break;
                            case 17:
                            if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {					//IOvalue 18
                                if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                    oSPBean.setShBackupPwrsupp((short)1);
                                if(sETD[0]!=null && !sETD[0].isEmpty())
                                {
                                    oSPBean.setdBackVol(Double.parseDouble(sETD[0].split(":")[1])*1000);
                                }
                            }
                            break;
                            case 18:
                            	if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 19
                                    if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                        oSPBean.setShADC1Status((short)1);
                                    oSPBean.setdADC1Vol(Double.parseDouble(sETD[0].split(":")[1])*1000);
                                }
                                break;
                            case 19:
                            	if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 20
                                    if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)	
                                        oSPBean.setShADC2Status((short)1);
                                    oSPBean.setdADC2Vol(Double.parseDouble(sETD[0].split(":")[1])*1000);
                                }
                                break;
                            case 20:
                            	if (hOldIOvalues.containsKey((int)-9)) {					//IOvalue -9
                                    if (hOldIOvalues.get(-9) == 0)
                                        oSPBean.setShSleepMode((short)1);
                                }
                                break;
                            case 28:
                            	if (hOldIOvalues.containsKey((int) 21)) {					//IOvalue 21
                                    if (hOldIOvalues.get(21) == 0)
                                        oSPBean.setShAccOn((short)1);
                                }
                                break;
                                //shParking=
                        }


                    }
                }
            }/*
                        iMCC1 = Integer.parseInt(sGSMDeviceData[2]);
                        iMNC1 = Integer.parseInt(sGSMDeviceData[3]);
                        iLAC1 = Integer.parseInt(sGSMDeviceData[4], 16);
                        iCID1 = Integer.parseInt(sGSMDeviceData[5], 16);
                        iRSS1 = Integer.parseInt(sGSMDeviceData[6]);
                    }
                    else if(sGSMDeviceData.length<12) {
                        iMCC2 = Integer.parseInt(sGSMDeviceData[7]);
                        iMNC2 = Integer.parseInt(sGSMDeviceData[8]);
                        iLAC2 = Integer.parseInt(sGSMDeviceData[9], 16);
                        iCID2 = Integer.parseInt(sGSMDeviceData[10], 16);
                        iRSS2 = Integer.parseInt(sGSMDeviceData[11]);
                    }
                    else if(sGSMDeviceData.length>12) {
                        iMCC3 = Integer.parseInt(sGSMDeviceData[12]);
                        iMNC3 = Integer.parseInt(sGSMDeviceData[13]);
                        iLAC3 = Integer.parseInt(sGSMDeviceData[14], 16);
                        iCID3 = Integer.parseInt(sGSMDeviceData[15], 16);
                        iRSS3 = Integer.parseInt(sGSMDeviceData[16]);
                    }
                }
                else if(sCommand.equals("COT")) {
                    sCOTDeviceData = sDeviceData[7].split(";");
                    lOdom = Long.parseLong(sCOTDeviceData[0].split(":")[1]);
                    lIN1= Long.parseLong(sCOTDeviceData[2].substring(2,sCOTDeviceData[2].length()));
                    lIN2= Long.parseLong(sCOTDeviceData[3].substring(2,sCOTDeviceData[3].length()));
                    lIN3= Long.parseLong(sCOTDeviceData[4].substring(2,sCOTDeviceData[4].length()));
                    lIN4=Long.parseLong(sCOTDeviceData[5].substring(2,sCOTDeviceData[5].length()));
                    System.out.println("Lin1 "+lIN1+" Lin2 "+lIN2+" Lin3 "+lIN3+" Lin4 "+lIN4);
                }
                else if(sCommand.equals("ADC")) {
                    sADCDeviceData = sDeviceData[8].split(";");
                    dExtVol = (Double.parseDouble(sADCDeviceData[0].split(":")[1]) * 1000);
                    dBackVol = (Double.parseDouble(sADCDeviceData[1])) * 1000;
                    dADC1Vol = (Double.parseDouble(sADCDeviceData[2])) * 1000;
                    dADC2Vol = (Double.parseDouble(sADCDeviceData[3])) * 1000;*/
         String sIoValues="1="+oSPBean.getShTow()+",2="+oSPBean.getShIdle()+",3="+oSPBean.getShParked()+",4="+oSPBean.getShOverSpeed()+",5="+oSPBean.getShAntiJam()+",6="+oSPBean.getShGeoFence()+",7="+oSPBean.getShFirstTimeReport()+",-9="+oSPBean.getShSleepMode()+",14="+oSPBean.getShIButton()+",15="+oSPBean.getShTemperature()+",16="+oSPBean.getShBalanceLow()+",17="+oSPBean.getShExtPwrsupp()+
                    ",18="+oSPBean.getShBackupPwrsupp()+",19="+oSPBean.getShADC1Status()+",20="+oSPBean.getShADC2Status()+",21="+oSPBean.getShAccOn()+",22="+oSPBean.getShWorkStatus()+",23="+oSPBean.getShInterRoaming()+",25="+oSPBean.getShDomesticRoaming()+",26="+oSPBean.getShDI1()+
                    ",27="+oSPBean.getShDI2()+",28="+oSPBean.getShDI3()+",29="+oSPBean.getShDI4()+",30="+oSPBean.getShDO1()+",31="+oSPBean.getShDO2()+",32="+oSPBean.getShD03()+",33="+oSPBean.getShSocket1()+",34="+oSPBean.getShSocket2()+",35="+oSPBean.getShSocket3()+",36="+oSPBean.getShSocket4()+",37="+oSPBean.getShSocket5()+",38="+oSPBean.getShMotionSensor()+
                    ",39="+oSPBean.getShFlashStorage()+",40="+oSPBean.getShVibrationSensor()+",42="+oSPBean.getdVDOP()+",43="+oSPBean.getiMCC1()+",44="+oSPBean.getiMNC1()+",45="+oSPBean.getiRSS1()+",46="+oSPBean.getiMCC2()+",47="+oSPBean.getiMNC2()+",48="+oSPBean.getiLAC2()+
                    ",49="+oSPBean.getiCID2()+",50="+oSPBean.getiRSS2()+",51="+oSPBean.getiMCC3()+",52="+oSPBean.getiMNC3()+",53="+oSPBean.getiLAC3()+",54="+oSPBean.getiCID3()+",55="+oSPBean.getiRSS3()+",255="+oSPBean.getShAccident()+",253="+oSPBean.getShHarshBehavior()+",257="+oSPBean.getdXAxis()+",258="+oSPBean.getdYAxis()+
                    ",56="+oSPBean.getlIN1()+",57="+oSPBean.getlIN2()+",58="+oSPBean.getlIN3()+",59="+oSPBean.getlIN4()+",60="+oSPBean.getdADC1Vol()+",61="+oSPBean.getdADC2Vol()+",987="+oSPBean.getdExtVol()+",988="+oSPBean.getdBackVol()+",78="+oSPBean.getsRfid()+
                    ",260="+oSPBean.getiLAC1()+",261="+oSPBean.getiCID1()+",24="+oSPBean.getIspeed()+",240="+oSPBean.getShMoving()+",985="+oSPBean.getdHDOP()+",997="+oSPBean.getiGSM()+",998="+(oSPBean.isbGPSValidity()==true?1:0)+"";
         int lMainTimeStamp=0;
         String alarmId=(lMainTimeStamp < oSPBean.getlFixtime()) ? getEvent(sIoValues,sOldIOValues,new int[]{1,2,3,4,5,6,7,-9,14,15,16,17,18,19,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,255,253,240}) : "0";
            System.out.println("alarm id "+alarmId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
       /* System.out.println("Temperature is"+dTemp);
        System.out.println(lImei);
        System.out.println(sDate);
        System.out.println(sTime);
        System.out.println(lFixtime);
        System.out.println(bGPSValidity);
        System.out.println(dLatitude);
        System.out.println(dLongitude);
        System.out.println(ispeed);
        System.out.println(iSatelites);
        System.out.println(iAltitude);
        System.out.println(iAzimuth);
        System.out.println("iLAC3"+iLAC3);
        System.out.println("iCID3"+iCID3);
        System.out.println(iEvent);*/

        int i=0;
        for(String s: sDeviceData)
        {
            System.out.println(i+++"     "+s);
        }
        DecodeHex(null,0);
    }
    public static String getEvent(String sIoValues,String sOldIoValues,int[] iDigitalPort){
        String sEvent="0";
        try
        {
            Hashtable htIoValues=new Hashtable(), htOldIoValues=new Hashtable();

            String[] sIoValue = sIoValues.split(",");
            for(int iter=0;iter<sIoValue.length;iter++){
                try{
                	System.out.println(sIoValue[iter]);
                    htIoValues.put(sIoValue[iter].split("=")[0],sIoValue[iter].split("=")[1]);
                }catch(Exception e){e.printStackTrace();}
            }
            sIoValue = sOldIoValues.split(",");
            for(int iter=0;iter<sIoValue.length;iter++){
                try{
                    htOldIoValues.put(sIoValue[iter].split("=")[0],sIoValue[iter].split("=")[1]);
                }
                catch(Exception e){e.printStackTrace();}
            }

            int iDigitalInput,iOldDigitalInput;
            for(int iter=0;iter<iDigitalPort.length;iter++)
            {
                if(htIoValues.containsKey(""+iDigitalPort[iter])){
                    iDigitalInput=Integer.parseInt(""+htIoValues.get(""+iDigitalPort[iter]));
                }
                else{
                    continue;
                }

                if(htOldIoValues.containsKey(""+iDigitalPort[iter])){
                    iOldDigitalInput=Integer.parseInt(""+htOldIoValues.get(""+iDigitalPort[iter]));
                }
                else{
                    continue;
                }
                if(iDigitalInput != iOldDigitalInput){
                    if(sEvent.equals("0"))
                        sEvent = ""+iDigitalPort[iter];
                    else
                        sEvent += ","+iDigitalPort[iter];
                }
            }
        }
        catch(Exception e){
            StackTraceElement[] stacktraceElements = e.getStackTrace();
            for(int iter=0;iter<stacktraceElements.length;iter++){
               // Debug.print("Class ("+stacktraceElements[iter].getClassName()+")--- Method( "+stacktraceElements[iter].getMethodName()+")--- Line Number("+stacktraceElements[iter].getLineNumber()+")","info");
            }
            e.printStackTrace();
        }
        return sEvent;
    }
    private static void DecodeHex(byte[] baData,int iLength)
    {
    	SP3600Bean oSPBean=new SP3600Bean();
    	String sSP3600="",sByte="";
		Byte byteData;
		//String [] sData = new String[iLength];
    	for(int iter=0;iter<iLength;iter++){
			byteData = new Byte(baData[iter]);
			sByte = byteData.intValue() < 9 && byteData.intValue() >= 0 ? "0"+Integer.toHexString(byteData.intValue()) : Integer.toHexString(byteData.intValue());
			sByte = sByte.replaceAll("ffffff","");
			sByte = (sByte.length()==1?"0"+sByte:sByte);
			//sData[iter] = sByte;
			sSP3600 += " "+sByte;
		}
    	//String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"9C" ,"00" ,"FB" ,"11" ,"03" ,"47" ,"36" ,"53" ,"15" ,"56" ,"32" ,"2E" ,"32" ,"35" ,"26" ,"56" ,"31" ,"2E" ,"30" ,"2E" ,"33" ,"15" ,"00" ,"3F" ,"48" ,"01" ,"66" ,"C3" ,"B0" ,"06" ,"BC" ,"3E" ,"40" ,"00" ,"00" ,"00" ,"00" ,"03" ,"E8" ,"00" ,"64" ,"06" ,"02" ,"FF" ,"7F" ,"12" ,"07" ,"AD" ,"08" ,"03" ,"2D" ,"11" ,"74" ,"21" ,"74" ,"31" ,"74" ,"0B" ,"01" ,"02" ,"11" ,"E1" ,"21" ,"00" ,"31" ,"00" ,"42" ,"40" ,"00" ,"08" ,"07" ,"00" ,"00", "00" ,"00" ,"12" ,"03" ,"8C" ,"08" ,"07" ,"00" ,"00" ,"00" ,"00" ,"12" ,"03" ,"8C" ,"6F" ,"89" ,"F8" };
    	//String sData[]= {"F8","06","01","01","44","3B","33","F7","8B","E4","19","46","E3","3E","00","33","10","03","47","36","53","15","56","31","2E","30","30","25","56","31","2E","30","31","15","00","3F","49","01","61","79","2C","06","C2","C8","40","00","00","00","00","00","8B","00","86","04","03","1C","11","F4","09","02","30","8C","11","80","21","00","31","00","98","46","F8"
//};
    	//String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"03" ,"00" ,"FF","11","03","47","36","53","15","56","31","2E","30","30","26","56","68","31","2E","30","32","15","00","7F","48","01","61","75","DD","06","C2","C8","E8","00","00","00","00","00","4B","00","7C","00","7D","1A","0F","14","1C","C0","00","25","03","96","2C","40","1C","C0","00","27","31","40","F4","56","1C","C0","00","27","31","BB","42","66","0B","02","03","E8","12","97","C2","24","20","00","03","E8","08","03","20","12","08","23","24","32","08","0E","01","00","11","C3","21","00","31","00","43","02","00","00","51","01","0A","19","01","00","00","00","12","03","8C","FE","E3","03","02","00","6D","6F" ,"89" ,"F8"};
        //String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"07" ,"00" ,"FF","11","03","47","36","53","15","56","31","2E","30","30","26","56","68","31","2E","30","32","15","00","7F","48","01","61","75","DD","06","C2","C8","E8","00","00","00","00","00","4B","00","7C","00","7D","1A","0F","14","1C","C0","00","25","03","96","2C","40","1C","C0","00","27","31","40","F4","56","1C","C0","00","27","31","BB","42","66","0B","02","03","E8","12","97","C2","24","20","00","03","E8","08","03","20","12","08","23","24","32","08","0E","01","00","11","C3","21","00","31","00","43","02","00","00","51","01","0A","19","01","00","00","00","12","03","8C","FE","E3","02","01","44","6F" ,"89" ,"F8"};
       String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"0A" ,"00" ,"FF","11","03","47","36","53","15","56","31","2E","30","30","26","56","68","31","2E","30","32","15","00","7F","48","01","61","75","DD","06","C2","C8","E8","00","00","00","00","00","4B","00","7C","00","7D","1A","0F","14","1C","C0","00","25","03","96","2C","40","1C","C0","00","27","31","40","F4","56","1C","C0","00","27","31","BB","42","66","0B","02","03","E8","12","97","C2","24","20","00","03","E8","08","03","20","12","08","23","24","32","08","0E","01","00","11","C3","21","00","31","00","43","02","00","00","51","01","0A","19","01","00","00","00","12","03","8C","FE","E3","05","04","FF","9C","00","D7","6F" ,"89" ,"F8"};
          //String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"0E" ,"00" ,"FF","11","03","47","36","53","15","56","31","2E","30","30","26","56","68","31","2E","30","32","15","00","7F","48","01","61","75","DD","06","C2","C8","E8","00","00","00","00","00","4B","00","7C","00","7D","1A","0F","14","1C","C0","00","25","03","96","2C","40","1C","C0","00","27","31","40","F4","56","1C","C0","00","27","31","BB","42","66","0B","02","03","E8","12","97","C2","24","20","00","03","E8","08","03","20","12","08","23","24","32","08","0E","01","00","11","C3","21","00","31","00","43","02","00","00","51","01","0A","19","01","00","00","00","12","03","8C","FE","E3","0A","19","01","00","00","00","12","03","8C","00","64" ,"6F","89" ,"F8"};
        //String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"0E" ,"00" ,"FF","11","03","47","36","53","15","56","31","2E","30","30","26","56","68","31","2E","30","32","15","00","7F","48","01","61","75","DD","06","C2","C8","E8","00","00","00","00","00","4B","00","7C","00","7D","1A","0F","14","1C","C0","00","25","03","96","2C","40","1C","C0","00","27","31","40","F4","56","1C","C0","00","27","31","BB","42","66","0B","02","03","E8","12","97","C2","24","20","00","03","E8","08","03","20","12","08","23","24","32","08","0E","01","00","11","C3","21","00","31","00","43","02","00","00","51","01","0A","19","01","00","00","00","12","03","8C","FE","E3","08","07","00","00","00","00","13","3B","29","6F","89" ,"F8"};
        //String sData[]= {"F8" ,"06","41" ,"01" ,"44" ,"3B" ,"33" ,"F9" ,"0C" ,"28" ,"19" ,"A8" ,"DD" ,"F1" ,"10" ,"00" ,"FF","11","03","47","36","53","15","56","31","2E","30","30","26","56","68","31","2E","30","32","15","00","7F","48","01","61","75","DD","06","C2","C8","E8","00","00","00","00","00","4B","00","7C","00","7D","1A","0F","14","1C","C0","00","25","03","96","2C","40","1C","C0","00","27","31","40","F4","56","1C","C0","00","27","31","BB","42","66","0B","02","03","E8","12","97","C2","24","20","00","03","E8","08","03","20","12","08","23","24","32","08","0E","01","00","11","C3","21","00","31","00","43","02","00","00","51","01","0A","19","01","00","00","00","12","03","8C","FE","E3","03","02","02","BE","6F","89" ,"F8"};



       
    	boolean isEvent=false;
    	boolean isDevicePacket=false;
    	String sTypesofData="";
    	int iter=0;
    	
        String sOldIOValues="1=0,2=0,3=0,4=0,5=0,6=0,7=0,-9=0,14=0,15=0,16=0,17=0,18=0,19=0,20=0,21=0,22=0,23=0,25=0,26=0,27=0,28=0,29=0,30=0,31=0,32=0,33=0,34=0,35=0,36=0,37=0,38=0,39=0,40=0,42=0,43=0,44=0,45=0,46=0,47=0,48=0,49=0,50=0,51=0,52=0,53=0,54=0,55=0,255=0,253=0,257=0,258=0"
                +",56=0,57=0,58=0,59=0,60=0,61=0,987=0,988=0,78=0"
                +",260=0,261=0,24=0,240=0,985=0,997=0,998=0";
        Hashtable<Integer,Integer> hOldIOvalues=new Hashtable<Integer,Integer>();
        String aTemp[]=sOldIOValues.split(",");

        try
    	{
            for(int i=0;i<aTemp.length;)
            {
                hOldIOvalues.put(Integer.parseInt(aTemp[i].split("=")[0]),Integer.parseInt(aTemp[i].split("=")[1]));


                i=i+1;
            }
            oSPBean.setlImei(Long.parseLong(sData[3]+sData[4]+sData[5]+sData[6]+sData[7]+sData[8]+sData[9],16));
            oSPBean.setlFixtime((Long.parseLong(sData[10]+sData[11]+sData[12]+sData[13],16)+946684800)*1000);
    		String sTemp=HexToBinary(sData[2]);
    		isEvent=sTemp.charAt(1)=='1'?true:false;
    		isDevicePacket=sData[2].charAt(1)=='1'?true:false;
    		iter=14;
    		System.out.println("isEVENT"+isEvent);
    		if(isEvent)
    		{
    			String sTemp1=HexToBinary(sData[iter]);
    			iter++;
    			oSPBean.setiEvent(Integer.parseInt(sTemp1.substring(1, sTemp1.length()),2));
    		}
    		sTypesofData=HexToBinary(sData[iter]+sData[iter+1]);
    		iter=iter+2;
    		
    		if(sTypesofData.charAt(7)=='1') 	//SysData
    		{
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println("gps data will be"+iter+" "+sData[iter]);
    		}
    		if(sTypesofData.charAt(6)=='1')	//GPS
    		{
    			int tempIter=iter;
    			//System.out.println(tempIter);
    			tempIter++;
    			tempIter=tempIter+2;
    			String sTempFix=HexToBinary(sData[tempIter]);
    			System.out.println(sData[tempIter]+" "+sTempFix+"  "+sTempFix.substring(3,sTempFix.length()));
    			oSPBean.setiSatelites(Integer.parseInt(sTempFix.substring(3, sTempFix.length()),2));
    			System.out.println(Integer.parseInt(sTempFix.substring(0,3),2));
    			oSPBean.setbGPSValidity(Integer.parseInt(sTempFix.substring(0,3),2)==0?false:true);
    			System.out.println("gps"+oSPBean.isbGPSValidity());	
    				
    			oSPBean.setdLatitude(HexToLatitudeAndLongitude(sData[tempIter+1]+sData[tempIter+2]+sData[tempIter+3]+sData[tempIter+4]));
    			oSPBean.setdLongitude(HexToLatitudeAndLongitude(sData[tempIter+5]+sData[tempIter+6]+sData[tempIter+7]+sData[tempIter+8]));
    			oSPBean.setIspeed(Integer.parseInt(sData[tempIter+9]+sData[tempIter+10],16));
    			oSPBean.setiAzimuth(Integer.parseInt(sData[tempIter+11]+sData[tempIter+12],16));
    			oSPBean.setiAltitude(Integer.parseInt(sData[tempIter+13]+sData[tempIter+14],16));
    			oSPBean.setdHDOP(Integer.parseInt(sData[tempIter+15]+sData[tempIter+16],16)/100.0d);
    			oSPBean.setdVDOP(Integer.parseInt(sData[tempIter+17]+sData[tempIter+18],16)/100.0d);
    			System.out.println("GPS data"+sData[tempIter+18]+" "+(tempIter+18));
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    			
    			/*
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter-1]);*/
    		}
    		if(sTypesofData.charAt(5)=='1')			//GSM
    		{
    			int tempIter=iter;
    			oSPBean.setiGSM(Integer.parseInt(sData[tempIter+2].substring(1,sData[tempIter+2].length()),16));
                System.out.println("iGSM:::::::::::::::"+oSPBean.getiGSM());
    			int iCurrentBytes;
    			iCurrentBytes=2;
    			int bytesRead=Integer.parseInt(sData[tempIter],16);
    			System.out.println(bytesRead);
    			if(iCurrentBytes<bytesRead)
    			{
    				oSPBean.setiMCC1(Integer.parseInt(sData[tempIter + 3] + sData[tempIter + 4] + sData[tempIter + 5], 16));
    				oSPBean.setiLAC1(Integer.parseInt(sData[tempIter + 6] + sData[tempIter + 7], 16));
    				oSPBean.setiCID1(Integer.parseInt(sData[tempIter + 8] + sData[tempIter + 9], 16));
    				oSPBean.setiRSS1(-Integer.parseInt(sData[tempIter + 10], 16));
                    iCurrentBytes=iCurrentBytes+8;
                    System.out.println(oSPBean.getiMCC1()+"  "+oSPBean.getiLAC1()+"   "+oSPBean.getiCID1()+"  "+oSPBean.getiRSS1());
                }
                if(iCurrentBytes<bytesRead)
                {
                	oSPBean.setiMCC2(Integer.parseInt(sData[tempIter + 11] + sData[tempIter + 12] + sData[tempIter + 13], 16));
                	oSPBean.setiLAC2 (Integer.parseInt(sData[tempIter + 14] + sData[tempIter + 15], 16));
                	oSPBean.setiCID2 ( Integer.parseInt(sData[tempIter + 16] + sData[tempIter + 17], 16));
                	oSPBean.setiRSS2 ( -Integer.parseInt(sData[tempIter + 18], 16));
                    iCurrentBytes=iCurrentBytes+8;
                    System.out.println(oSPBean.getiMCC1()+"  "+oSPBean.getiLAC1()+"   "+oSPBean.getiCID1()+"  "+oSPBean.getiRSS1());

                }
                if(iCurrentBytes<bytesRead)
                {
                	oSPBean.setiMCC3 (Integer.parseInt(sData[tempIter + 19] + sData[tempIter + 20] + sData[tempIter + 21], 16));
                	oSPBean.setiLAC3 ( Integer.parseInt(sData[tempIter + 22] + sData[tempIter + 23], 16));
                	oSPBean.setiCID3 ( Integer.parseInt(sData[tempIter + 24] + sData[tempIter + 25], 16));
                	oSPBean.setiRSS3 ( -Integer.parseInt(sData[tempIter + 26], 16));
                    iCurrentBytes=iCurrentBytes+8;
                    System.out.println(oSPBean.getiMCC1()+"  "+oSPBean.getiLAC1()+"   "+oSPBean.getiCID1()+"  "+oSPBean.getiRSS1());

                }
                if(iCurrentBytes<bytesRead)
                {
                	iCurrentBytes=bytesRead;
                }
                    iter = iter + Integer.parseInt(sData[iter], 16) + 1;
                    System.out.println(iter+" "+sData[iter]);
    		}
    		if(sTypesofData.charAt(4)=='1')			//COT
    		{
    		    int tempIter=iter;
    		    int bytesRead=Integer.parseInt(sData[tempIter],16);
    		    System.out.println("Bytes Read are "+bytesRead);
    		    int iCurrentBytes=0;
    		    tempIter=tempIter+1;
                while (bytesRead > 0) {
                    int dataid = Integer.parseInt(sData[tempIter].substring(0, 1), 16);
                    System.out.println("DATAOID"+dataid);
                    int isubBytesRead = Integer.parseInt(sData[tempIter].substring(1, sData[tempIter].length()), 16);
                    bytesRead=bytesRead-1-isubBytesRead;
                    tempIter = tempIter + 1;
                    if(dataid==0) {
                        System.out.println("Sub bytes are" + isubBytesRead);
                        String lodom1 = "";

                        while (isubBytesRead > 0) {
                            System.out.println("Isnide lodom");
                            lodom1 = lodom1 + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        //tempIter=tempIter+1;
                        oSPBean.setlOdom(Long.parseLong(lodom1, 16));
                    }
                    else if(dataid==1)
                    {

                        while (isubBytesRead>0)
                        {
                            tempIter=tempIter+1;
                            isubBytesRead--;
                        }
                    }

                    else if(dataid==2)
                    {
                        String LIN1="";

                        while (isubBytesRead>0)
                        {
                            System.out.println("Isnide lodom");
                            LIN1 = LIN1 + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        System.out.println("String is "+LIN1);
                        oSPBean.setlIN1(Long.parseLong(LIN1.substring(1,LIN1.length()),16));
                    }
                    else if(dataid==3)
                    {
                        String LIN2="";

                        while (isubBytesRead>0)
                        {
                            System.out.println("Isnide lodom");
                            LIN2 = LIN2 + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        System.out.println("String is "+LIN2);
                        oSPBean.setlIN1(Long.parseLong(LIN2.substring(1,LIN2.length()),16));
                    }
                    else if(dataid==4)
                    {
                        String LIN3="";

                        while (isubBytesRead>0)
                        {
                            System.out.println("Isnide lodom");
                            LIN3 = LIN3 + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        System.out.println("String is "+LIN3);
                        oSPBean.setlIN3(Long.parseLong(LIN3.substring(1,LIN3.length()),16));
                    }
                    else if(dataid==5)
                    {
                        String LIN4="";

                        while (isubBytesRead>0)
                        {
                            System.out.println("Isnide lodom");
                            LIN4 = LIN4 + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        System.out.println("String is "+LIN4);
                        oSPBean.setlIN4(Long.parseLong(LIN4.substring(1,LIN4.length()),16));
                    }
                    System.out.println("Bytes after 1 iter"+bytesRead+" temp should be"+tempIter);

                }
                System.out.println("Odometer" + oSPBean.getlOdom());
                System.out.println("Lin1"+oSPBean.getlIN1());
                System.out.println("Lin2"+oSPBean.getlIN2());
                System.out.println("Lin3"+oSPBean.getlIN3());
                System.out.println("Lin4"+oSPBean.getlIN4());
                System.out.println(sData[tempIter]);
                iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    			
    		}
    		if(sTypesofData.charAt(3)=='1')			//AD
    		{
    		    int tempIter=iter;
                int bytesRead=Integer.parseInt(sData[tempIter],16);
                tempIter=tempIter+1;
                System.out.println(bytesRead);
                while(bytesRead>0)
                {
                    int dataid=Integer.parseInt(sData[tempIter].substring(0,1),16);
                    if(dataid==0)
                    {
                        String sTempval=sData[tempIter].substring(1,sData[tempIter].length())+sData[++tempIter];
                        oSPBean.setdExtVol(HextoDecVolt(sTempval)*1000);
                    }
                    else if(dataid==1)
                    {
                        String sTempval=sData[tempIter].substring(1,sData[tempIter].length())+sData[++tempIter];
                        oSPBean.setdBackVol(HextoDecVolt(sTempval)*1000);
                    }
                    else  if (dataid==2)
                    {
                        String sTempval=sData[tempIter].substring(1,sData[tempIter].length())+sData[++tempIter];
                        oSPBean.setdADC1Vol(HextoDecVolt(sTempval)*1000);
                    }
                    else if(dataid==3)
                    {
                        String sTempval=sData[tempIter].substring(1,sData[tempIter].length())+sData[++tempIter];
                        oSPBean.setdADC2Vol(HextoDecVolt(sTempval)*1000);
                    }
                    bytesRead=bytesRead-2;
                    tempIter=tempIter+1;
                }
                System.out.println("External voltage"+oSPBean.getdExtVol());
                System.out.println("backup voltage"+oSPBean.getdBackVol());
                System.out.println("ADC1 voltage"+oSPBean.getdADC1Vol());
                System.out.println("ADC2 voltage"+oSPBean.getdADC2Vol()+"  Data iter"+sData[tempIter]);
                iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    		}
    		if(sTypesofData.charAt(2)=='1')			//DTT
    		{
    		    int tempIter=iter;
    		    int bytesRead=Integer.parseInt(sData[tempIter++],16);
    		    System.out.println("Start Read "+bytesRead);
    		    while (bytesRead>0)
                {
                    int dataid = Integer.parseInt(sData[tempIter].substring(0, 1), 16);
                    System.out.println("DATAOID"+dataid);
                    int isubBytesRead = Integer.parseInt(sData[tempIter].substring(1, sData[tempIter].length()), 16);
                    int bytesFlag=isubBytesRead;
                    bytesRead=bytesRead-1-isubBytesRead;
                    tempIter=tempIter+1;
                    if(dataid==0)
                    {
                        System.out.println("Sub bytes are" + isubBytesRead);
                        String sDeviceStatus= "";

                        while (isubBytesRead > 0) {
                            System.out.println("Isnide Device status");
                            sDeviceStatus = sDeviceStatus + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        String sDeviceStatus1=HexToBinary(sDeviceStatus);
                        System.out.println(""+sDeviceStatus+"device Status:   "+sDeviceStatus1);
                        oSPBean.setShWorkStatus((short) (sDeviceStatus1.charAt(0) == '1' ? 1 : 0));
                        oSPBean.setShParked((short) (sDeviceStatus1.charAt(1) == '1' ? 0 : 1));
                        oSPBean.setShADC1Status(((short) (sDeviceStatus1.charAt(2) == '1' ? 1 : 0)));
                        oSPBean.setShADC2Status(( (short) (sDeviceStatus1.charAt(3) == '1' ? 1 : 0)));
                        oSPBean.setShGeoFence((short) (sDeviceStatus1.charAt(4) == '1' ? 1 : 0));
                        oSPBean.setShAccident( (short) (sDeviceStatus1.charAt(5) == '1' ? 1 : 0));
                        //shHarshBehavior = (short) (sDeviceStatus1.charAt(6) == '1' ? 1 : 0);
                        oSPBean.setShInterRoaming((short) (sDeviceStatus1.charAt(7) == '1' ? 1 : 0));
                        if(bytesFlag>1) {
                        	oSPBean.setShDomesticRoaming((short) (sDeviceStatus1.charAt(8) == '1' ? 1 : 0));
                        	oSPBean.setShAntiJam( (short) (sDeviceStatus1.charAt(9) == '1' ? 1 : 0));
                        	oSPBean.setShTow( (short) (sDeviceStatus1.charAt(10) == '1' ? 1 : 0));
                        	oSPBean.setShIdle( (short) (sDeviceStatus1.charAt(11) == '1' ? 1 : 0));
                        	oSPBean.setShOverSpeed( (short) (sDeviceStatus1.charAt(12) == '1' ? 1 : 0));
                        	oSPBean.setShMoving( (short) (sDeviceStatus1.charAt(13) == '1' ? 1 : 0));
                        	oSPBean.setShBackupPwrsupp( (short) (sDeviceStatus1.charAt(14) == '1' ? 1 : 0));
                        	oSPBean.setShExtPwrsupp((short) (sDeviceStatus1.charAt(15) == '1' ? 1 : 0));
                        }
                        //break;

                    }
                    else if(dataid==1)
                    {
                        System.out.println("Sub bytes are" + isubBytesRead);
                        String sDeviceStatus= "";

                        while (isubBytesRead > 0) {
                            System.out.println("Isnide Device status");
                            sDeviceStatus = sDeviceStatus + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        String sDeviceStatus1=HexToBinary(sDeviceStatus);
                        System.out.println(""+sDeviceStatus+"device Status:   "+sDeviceStatus1);
                        oSPBean.setShAccOn((short) (sDeviceStatus1.charAt(0) == '1' ? 1 : 0));
                        oSPBean.setShDI1((short) (sDeviceStatus1.charAt(1) == '1' ? 0 : 1));
                        oSPBean.setShDI2( (short) (sDeviceStatus1.charAt(2) == '1' ? 0 : 1));
                        oSPBean.setShDI3( (short) (sDeviceStatus1.charAt(3) == '1' ? 1 : 0));
                        oSPBean.setShDI4( (short) (sDeviceStatus1.charAt(4) == '1' ? 1 : 0));
                        oSPBean.setShDO1( (short) (sDeviceStatus1.charAt(5) == '1' ? 1 : 0));
                        oSPBean.setShDO2((short) (sDeviceStatus1.charAt(6) == '1' ? 1 : 0));
                        oSPBean.setShD03( (short) (sDeviceStatus1.charAt(7) == '1' ? 1 : 0));
                       // break;
                    }
                    else if(dataid==2)                                  //GEOFENCE STATUS BLOCK
                    {
                        while (isubBytesRead>0)
                        {
                            tempIter=tempIter+1;
                            isubBytesRead--;
                        }
                    }
                    else if(dataid==3)                                  //Geofence STATUS BLOCK
                    {
                        while (isubBytesRead>0)
                        {
                            tempIter=tempIter+1;
                            isubBytesRead--;
                        }
                    }
                    else if(dataid==4)                                  //EVENT STATUS BLOCK
                    {
                        while (isubBytesRead>0)
                        {
                            tempIter=tempIter+1;
                            isubBytesRead--;
                        }
                    }
                    else if(dataid==5)                                  //PACKET TYPE INDICATOR
                    {
                        while (isubBytesRead>0)
                        {
                            tempIter=tempIter+1;
                            isubBytesRead--;
                        }
                    }
                    System.out.println("bytes read "+bytesRead);

                }
                System.out.println("After "+sData[tempIter]);
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    		}
    		if(sTypesofData.charAt(1)=='1')			//iWD
    		{
                int tempIter=iter;
                int bytesRead=Integer.parseInt(sData[tempIter],16);
                while (bytesRead>0)
                {
                    int dataid=Integer.parseInt(sData[++tempIter].substring(0,1),16);
                    int isubBytesRead=Integer.parseInt(sData[tempIter].substring(1,sData[tempIter].length()),16);
                    System.out.println("sub data bytes is "+isubBytesRead);
                    String sDataType=sData[++tempIter];
                    System.out.println(sDataType);
                    bytesRead=bytesRead-1-isubBytesRead;
                    isubBytesRead--;
                    tempIter=tempIter+1;
                    System.out.println("TEGe" +sData[tempIter]);
                    if(sDataType.equals("00"))
                    {
                        String iSerial="";
                        while (isubBytesRead>0)
                        {
                            iSerial = iSerial + sData[tempIter];
                            tempIter = tempIter + 1;
                            isubBytesRead--;
                        }
                        oSPBean.setsRfid(iSerial);

                    }
                    else if(sDataType.equals("01")) {
                        String iTemp = "";
                        while (isubBytesRead > 0) {
                            if (isubBytesRead < 3) {

                                iTemp = iTemp + sData[tempIter];
                            }

                            tempIter = tempIter + 1;
                            isubBytesRead--;

                        }
                        System.out.println("Temperature is "+iTemp);
                        if (Integer.parseInt(iTemp.substring(0, 1), 16) > 7) {
                            String sTemp1 = HexToBinary(iTemp);
                            String sTemp12 = "";
                            for (int i = 0; i < sTemp1.length(); i++) {
                                if (sTemp1.charAt(i) == '1') {
                                    sTemp12 = sTemp12 + '0';
                                } else {
                                    sTemp12 = sTemp12 + '1';
                                }
                            }
                            oSPBean.setdTemp( -(Integer.parseInt(sTemp12, 2) + 1)/10.0d);
                        } else {
                        	oSPBean.setdTemp(Integer.parseInt(iTemp,16)/10.0d) ;
                        }
                    }
                }
                System.out.println("Temp is "+oSPBean.getdTemp()+"  Data is: "+sData[tempIter]);

    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    		}
    		if(sTypesofData.charAt(0)=='1')			//ETD
    		{
    		    String sETD[]={"",""};
    		    double dCommonVoltage=0.0d;
    		    System.out.println("EVENT ID"+oSPBean.getiEvent());
                if (oSPBean.getiEvent() != -1) {
                    String sXAxis="";
                    String sYAxis="";
                    if(oSPBean.getiEvent()>7 && oSPBean.getiEvent()<14)
                    {
                        int tempIter=iter;
                        int bytesRead=Integer.parseInt(sData[tempIter],16);
                        System.out.println("bytes read Harsh "+bytesRead);
                        tempIter=tempIter+1;
                        while (bytesRead>0)
                        {
                            int isubBytes=Integer.parseInt(sData[tempIter],16);
                            tempIter=tempIter+1;
                            bytesRead=bytesRead-1-isubBytes;
                            while(isubBytes>0)
                            {
                                if(isubBytes>2)
                                    sXAxis=sXAxis+sData[tempIter];
                                else
                                    sYAxis=sYAxis+sData[tempIter];
                                tempIter=tempIter+1;
                                isubBytes--;
                            }
                        }
                        if(Integer.parseInt(sXAxis.substring(0,1),16)>7)
                        {
                            String sTmp=onesComplement(sXAxis);
                            System.out.println("STMP: "+sTmp);
                            oSPBean.setdXAxis(-Integer.parseInt(sTmp,2)/1000.0d);
                        }
                        else
                        {
                        	oSPBean.setdXAxis(Integer.parseInt(sXAxis,16)/1000.0d);
                        }
                        if(Integer.parseInt(sYAxis.substring(0,1),16)>7)
                        {
                            String sTmp=onesComplement(sYAxis);
                            oSPBean.setdYAxis(-(Integer.parseInt(sTmp,2)/1000.0d));
                        }
                        else
                        {
                        	oSPBean.setdYAxis(Integer.parseInt(sYAxis,16)/1000.0d);
                        }
                        System.out.println("XAXIS "+oSPBean.getdXAxis()+"   YAXIS "+oSPBean.getdYAxis());
                        System.out.println("XAxisandYaxis:  "+sXAxis+"   "+sYAxis);

                    }
                    else if(oSPBean.getiEvent()>15 && oSPBean.getiEvent()<20)
                    {
                        System.out.println("VOLTAGE EVENT: "+oSPBean.getiEvent());
                        int tempIter=iter;
                        String sVoltage="";
                        System.out.println("Data is:  "+sData[tempIter]);
                        int bytesRead=Integer.parseInt(sData[tempIter],16);
                        tempIter=tempIter+1;
                        while (bytesRead>0)
                        {
                            int isubBytesRead=Integer.parseInt(sData[tempIter].substring(1,sData[tempIter].length()),16);
                            System.out.println("Sub bytes read: "+isubBytesRead);
                            System.out.println("Data is: "+sData[tempIter]);
                            sVoltage="";
                            bytesRead=bytesRead-1-isubBytesRead;
                            tempIter++;
                            while(isubBytesRead>0)
                            {
                                sVoltage=sVoltage+sData[tempIter++];
                                isubBytesRead--;
                            }
                        }
                        dCommonVoltage=HextoDecVolt(sVoltage)*1000;
                        System.out.println(sVoltage+"  "+dCommonVoltage);
                    }
                    switch (oSPBean.getiEvent()){
                        case 0:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1)) 	//IOvalue 1
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShTow((short) 1);
                            }
                            break;
                        case 1:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 2
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShIdle((short) 1);
                            }
                            break;
                        case 2:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 3
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShParked((short) 1);
                            }
                            break;
                        case 3:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 4
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShOverSpeed(((short) 1));;
                                int tempIter=iter;

                                int bytesRead=Integer.parseInt(sData[tempIter],16);
                                while(bytesRead>0) {
                                    tempIter=tempIter+1;
                                    int isubBytes=Integer.parseInt(sData[tempIter].substring(1,sData[tempIter].length()),16);
                                    System.out.println("is sub bytes "+isubBytes);
                                    String sSpeed = "";
                                    tempIter = tempIter + 1;
                                    bytesRead=bytesRead-1-isubBytes;
                                    while (isubBytes > 0) {
                                        //System.out.println("data is"+sData[tempIter]);
                                        sSpeed = sSpeed + sData[tempIter];
                                        tempIter=tempIter+1;
                                        isubBytes--;
                                    }
                                    System.out.println("speed is " + sSpeed);
                                    oSPBean.setIspeed(Integer.parseInt(sSpeed, 16));
                                    System.out.println("OverSpeed status: " + oSPBean.getiOverSpeedStatus()+" status "+oSPBean.getShOverSpeed());
                                } //iOverSpeedStatus=Integer.parseInt(sETD[0].split(":")[1]);
                            }
                            break;
                        case 4:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 5
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShAntiJam(((short) 1));;

                            }
                            break;
                        case 5:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 6
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShGeoFence(((short) 1));;

                            }
                            break;
                        case 6:
                            if(hOldIOvalues.containsKey((int)oSPBean.getiEvent()+1))		//IOvalue 7
                            {
                                if(hOldIOvalues.get(oSPBean.getiEvent()+1)==0)
                                	oSPBean.setShFirstTimeReport(((short) 1));;

                            }
                            break;
                        case 7:
                            int tempIter=iter;
                            int iBytesread=Integer.parseInt(sData[tempIter],16);
                            tempIter=tempIter+1;
                            String sHealthyCheck="";
                            while (iBytesread>0)
                            {
                                int isubBytesRead=Integer.parseInt(sData[tempIter].substring(1,sData[tempIter].length()));
                                System.out.println("Sub bytes is "+isubBytesRead);
                                tempIter=tempIter+1;
                                iBytesread=iBytesread-1-isubBytesRead;
                                while(isubBytesRead>0)
                                {
                                    sHealthyCheck=sHealthyCheck+sData[tempIter];
                                    tempIter=tempIter+1;
                                    isubBytesRead--;
                                }
                                System.out.println("Data string is "+sHealthyCheck);
                            }
                            String sTempStr=HexToBinary(sHealthyCheck);
                            System.out.println("Binary string is "+ sTempStr);

                            oSPBean.setShSocket1((short)(sTempStr.charAt(7)=='1'?0:1));
                            oSPBean.setShSocket2((short)(sTempStr.charAt(6)=='1'?0:1));
                            oSPBean.setShSocket3((short)(sTempStr.charAt(5)=='1'?0:1));
                            oSPBean.setShSocket4((short)(sTempStr.charAt(4)=='1'?0:1));
                            oSPBean.setShSocket5((short)(sTempStr.charAt(3)=='1'?0:1));
                            oSPBean.setShMotionSensor((short)(sTempStr.charAt(2)=='1'?0:1));
                            oSPBean.setShFlashStorage((short)(sTempStr.charAt(1)=='1'?0:1));
                            oSPBean.setShVibrationSensor((short)(sTempStr.charAt(0)=='1'?0:1));
                            System.out.println("shSocket1"+oSPBean.getShSocket1()+"shSocket2"+oSPBean.getShSocket2()+"shSocket3"+oSPBean.getShSocket3()+" shSocket4 "+oSPBean.getShSocket4()+" shSocket5 "+oSPBean.getShSocket5()+" shMotionSensor "+oSPBean.getShMotionSensor()+ " shFlashStorage "+oSPBean.getShFlashStorage()+" shVibrationSensor "+oSPBean.getShVibrationSensor());
                            break;
                        case 8:
                            if(hOldIOvalues.containsKey((int)253))		//IOvalue 253
                            {
                                if(hOldIOvalues.get(253)==0)
                                {
                                	oSPBean.setShHarshAccceleration((short)2);
                                	oSPBean.setShHarshBrake(((short) 1));;
                                }
                            }
                            break;
                        case 9:
                            if(hOldIOvalues.containsKey((int)253))			//IOvalue 253
                            {
                                if(hOldIOvalues.get(253)==0)
                                {  
                                	oSPBean.setShHarshBehavior(((short) 1));;
                                	oSPBean.setShHarshAccceleration(((short) 1));;
                                }

                            }
                            break;
                        case 10:
                            if(hOldIOvalues.containsKey((int)253))			//IOvalue 253
                            {
                                if(hOldIOvalues.get(253)==0)
                                {
                                	oSPBean.setShHarshBehavior((short) 3);
                                	oSPBean.setShHarshCornering((short) 1);
                                }
                            }
                            break;
                        case 11:
                            if(hOldIOvalues.containsKey((int)255))			//IOvalue 255
                            {
                                if(hOldIOvalues.get(255)==0)
                                {
                                	oSPBean.setShAccident(((short) 1));
                                	oSPBean.setShFrontCollision(((short) 1));
                                }


                            }
                            break;
                        case 12:
                            if(hOldIOvalues.containsKey((int)255))			//IOvalue 255
                            {
                                if(hOldIOvalues.get(255)==0)
                                {
                                	oSPBean.setShAccident(((short) 1));
                                	oSPBean.setShRearCollision(((short) 1));
                                }


                            }
                            break;
                        case 13:
                            if(hOldIOvalues.containsKey((int)255))			//IOvalue 255
                            {
                                if(hOldIOvalues.get(255)==0)
                                {
                                	oSPBean.setShAccident(((short) 1));
                                	oSPBean.setShTurnOver(((short) 1));
                                }
                            }
                            break;
                        case 14:
                            System.out.println("In the temp event"+oSPBean.getiEvent());
                            int tempIter1=iter;
                            System.out.println("Temp data is"+sData[tempIter1]);
                            int bytesRead=Integer.parseInt(sData[tempIter1],16);
                            System.out.println("bytes Read "+bytesRead);

                            while (bytesRead>0)
                            {
                                int dataid=Integer.parseInt(sData[++tempIter1].substring(0,1),16);
                                int isubBytesRead=Integer.parseInt(sData[tempIter1].substring(1,sData[tempIter1].length()),16);
                                System.out.println("sub data bytes is "+isubBytesRead);
                                String sDataType=sData[++tempIter1];
                                System.out.println(sDataType);
                                bytesRead=bytesRead-1-isubBytesRead;
                                isubBytesRead--;
                                tempIter1=tempIter1+1;
                                System.out.println("TEGe" +sData[tempIter1]);
                                if(sDataType.equals("00"))
                                {
                                    if (hOldIOvalues.containsKey((int) oSPBean.getiEvent())) {		//IOvalue 14

                                        if (hOldIOvalues.get(oSPBean.getiEvent()) == 0)
                                        	oSPBean.setShIButton(((short) 1));
                                    }
                                    String iSerial="";
                                    while (isubBytesRead>0)
                                    {
                                        iSerial = iSerial + sData[tempIter1];
                                        tempIter1 = tempIter1 + 1;
                                        isubBytesRead--;
                                    }
                                    oSPBean.setsRfid(iSerial);
                                    System.out.println("RFID "+oSPBean.getsRfid());
                                }
                                else if(sDataType.equals("01")) {
                                    if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {		//IOvalue 15
                                        if (hOldIOvalues.get(oSPBean.getiEvent() + 1) == 0)
                                        	oSPBean.setShTemperature(((short) 1));

                                    }
                                    String iTemp = "";
                                    while (isubBytesRead > 0) {
                                        if (isubBytesRead < 3) {

                                            iTemp = iTemp + sData[tempIter1];
                                        }

                                        tempIter1 = tempIter1 + 1;
                                        isubBytesRead--;

                                    }
                                    System.out.println("Temperature is "+iTemp);
                                    if (Integer.parseInt(iTemp.substring(0, 1), 16) > 7) {
                                        String sTemp1 = HexToBinary(iTemp);
                                        String sTemp12 = "";
                                        for (int i = 0; i < sTemp1.length(); i++) {
                                            if (sTemp1.charAt(i) == '1') {
                                                sTemp12 = sTemp12 + '0';
                                            } else {
                                                sTemp12 = sTemp12 + '1';
                                            }
                                        }
                                        oSPBean.setdTemp(-(Integer.parseInt(sTemp12, 2) + 1)/10.0d);
                                    } else {
                                    	oSPBean.setdTemp( Integer.parseInt(iTemp,16)/10.0d) ;
                                    }
                                }
                            }
                            System.out.println("Temp is "+oSPBean.getdTemp()+"  Data is: "+sData[tempIter1]);
                            break;
                        case 15:
                            if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 16
                                if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                	oSPBean.setShBalanceLow(((short) 1));
                            }
                            break;
                        case 16:
                            if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 17
                                if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                	oSPBean.setShExtPwrsupp((short) 1);

                                oSPBean.setdExtVol(dCommonVoltage);
                            }
                            break;
                        case 17:
                            if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {					//IOvalue 18
                                if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                	oSPBean.setShBackupPwrsupp((short) 1);
                                oSPBean.setdBackVol(dCommonVoltage);
                            }
                            break;
                        case 18:
                            if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 19
                                if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                	oSPBean.setShADC1Status((short) 1);
                                oSPBean.setdADC1Vol(dCommonVoltage);
                            }
                            break;
                        case 19:
                            if (hOldIOvalues.containsKey((int) oSPBean.getiEvent()+1)) {				//IOvalue 20
                                if (hOldIOvalues.get(oSPBean.getiEvent()+1) == 0)
                                	oSPBean.setShADC2Status((short) 1);
                                oSPBean.setdADC2Vol(dCommonVoltage);
                            }
                            break;
                        case 20:
                            if (hOldIOvalues.containsKey((int)-9)) {					//IOvalue -9
                                if (hOldIOvalues.get(-9) == 0)
                                	oSPBean.setShSleepMode((short) 1);
                            }
                            break;
                        case 28:
                            if (hOldIOvalues.containsKey((int) 21)) {					//IOvalue 21
                                if (hOldIOvalues.get(21) == 0)
                                	oSPBean.setShAccOn((short) 1);
                            }
                            break;
                        //shParking=
                    }


                }
    			iter=iter+Integer.parseInt(sData[iter],16)+1;
    			System.out.println(iter+" "+sData[iter]);
    		}
            String sIoValues="1="+oSPBean.getShTow()+",2="+oSPBean.getShIdle()+",3="+oSPBean.getShParked()+",4="+oSPBean.getShOverSpeed()+",5="+oSPBean.getShAntiJam()+",6="+oSPBean.getShGeoFence()+",7="+oSPBean.getShFirstTimeReport()+",-9="+oSPBean.getShSleepMode()+",14="+oSPBean.getShIButton()+",15="+oSPBean.getShTemperature()+",16="+oSPBean.getShBalanceLow()+",17="+oSPBean.getShExtPwrsupp()+
                    ",18="+oSPBean.getShBackupPwrsupp()+",19="+oSPBean.getShADC1Status()+",20="+oSPBean.getShADC2Status()+",21="+oSPBean.getShAccOn()+",22="+oSPBean.getShWorkStatus()+",23="+oSPBean.getShInterRoaming()+",25="+oSPBean.getShDomesticRoaming()+",26="+oSPBean.getShDI1()+
                    ",27="+oSPBean.getShDI2()+",28="+oSPBean.getShDI3()+",29="+oSPBean.getShDI4()+",30="+oSPBean.getShDO1()+",31="+oSPBean.getShDO2()+",32="+oSPBean.getShD03()+",33="+oSPBean.getShSocket1()+",34="+oSPBean.getShSocket2()+",35="+oSPBean.getShSocket3()+",36="+oSPBean.getShSocket4()+",37="+oSPBean.getShSocket5()+",38="+oSPBean.getShMotionSensor()+
                    ",39="+oSPBean.getShFlashStorage()+",40="+oSPBean.getShVibrationSensor()+",42="+oSPBean.getdVDOP()+",43="+oSPBean.getiMCC1()+",44="+oSPBean.getiMNC1()+",45="+oSPBean.getiRSS1()+",46="+oSPBean.getiMCC2()+",47="+oSPBean.getiMNC2()+",48="+oSPBean.getiLAC2()+
                    ",49="+oSPBean.getiCID2()+",50="+oSPBean.getiRSS2()+",51="+oSPBean.getiMCC3()+",52="+oSPBean.getiMNC3()+",53="+oSPBean.getiLAC3()+",54="+oSPBean.getiCID3()+",55="+oSPBean.getiRSS3()+",255="+oSPBean.getShAccident()+",253="+oSPBean.getShHarshBehavior()+",257="+oSPBean.getdXAxis()+",258="+oSPBean.getdYAxis()+
                    ",56="+oSPBean.getlIN1()+",57="+oSPBean.getlIN2()+",58="+oSPBean.getlIN3()+",59="+oSPBean.getlIN4()+",60="+oSPBean.getdADC1Vol()+",61="+oSPBean.getdADC2Vol()+",987="+oSPBean.getdExtVol()+",988="+oSPBean.getdBackVol()+",78="+oSPBean.getsRfid()+
                    ",260="+oSPBean.getiLAC1()+",261="+oSPBean.getiCID1()+",24="+oSPBean.getIspeed()+",240="+oSPBean.getShMoving()+",985="+oSPBean.getdHDOP()+",997="+oSPBean.getiGSM()+",998="+(oSPBean.isbGPSValidity()==true?1:0)+"";
         
            int lMainTimeStamp=0;
            String alarmId=(lMainTimeStamp < oSPBean.getlFixtime()) ? getEvent(sIoValues,sOldIOValues,new int[]{1,2,3,4,5,6,7,-9,14,15,16,17,18,19,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,255,253,240}) : "0";
            System.out.println("alarm id "+alarmId);
    			//lFixtime=(Long.parseLong("195A7F9E",16)+946684800)*1000;
    	System.out.println("Type of data"+sTypesofData);	
    	System.out.println(oSPBean.getlImei());
    	System.out.println(oSPBean.getlFixtime() );
    	System.out.println(sTemp);
    	System.out.println(isEvent);
    	System.out.println(isDevicePacket);
    	System.out.println(oSPBean.getiEvent());
    	System.out.println(oSPBean.getdLatitude());
    	System.out.println(oSPBean.getdLongitude());
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
    }

    private static double HextoDecVolt(String s)
    {
        int iTempvalue=Integer.parseInt(s,16);
        System.out.println("Temp voltage "+iTempvalue);
        return (iTempvalue*(110/4096.00d))-10.00d;
    }
    private static long _getUTCSeconds_YMD_HMS(String yymmdd, String hhmmss)
    {
        // 100406  021359
        if ((yymmdd.length() < 6) || (hhmmss.length() < 6)) {
            return 0L;
        } else {
            int  YY = Integer.parseInt(yymmdd.substring(0,2));
            int  MM = Integer.parseInt(yymmdd.substring(2,4));
            int  DD = Integer.parseInt(yymmdd.substring(4,6));
            int  hh = Integer.parseInt(hhmmss.substring(0,2));
            int  mm = Integer.parseInt(hhmmss.substring(2,4));
            int  ss = Integer.parseInt(hhmmss.substring(4,6));

            //System.out.println((new Date(YY,MM,DD,hh,mm,ss)).getTime());
            //this.sTimeStamp = (YY+2000)+"-"+(MM)+"-"+(DD<10 ? "0"+DD : DD)+" "+hh+":"+mm+":"+ss;
            if ((YY < 0) || (MM < 1) || (MM > 12) || (DD < 1) || (DD > 31) ||
                    (hh < 0) || (mm < 0) || (ss < 0)) {
                return 0L;
            } else {
                YY 	= YY+100;
                MM 	= MM-1;
                long lfixtime  = ((new Date(YY,MM,DD,hh,mm,ss)).getTime());
                return lfixtime;
            }
        }
    }
    public static String onesComplement(String s)
    {
        String sTemp="";
        try {
            s = HexToBinary(s);
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '1') {
                    sTemp = sTemp + '0';
                } else {
                    sTemp = sTemp + '1';
                }
            }
        }
        catch (Exception e )
        {
            e.printStackTrace();
        }
        return sTemp;

    }
    public static Double HexToLatitudeAndLongitude(String s)
    {
    	//1101 1000 0101 1001 0001 1101 000
    	//1111 1100 1011 0011 1110 1100 0101 1000
    	//String binString="01001000";
    	//1101 1000 01011 0010 00

    	//s="0";
    	//s="016175DD";
    	System.out.println(s);
    	String binString="";
		try {
			binString =HexToBinary(s);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	System.out.println("binString"+binString);
    	long dLatitudeandLng=0l;
    	String sTemp="";
    	try
    	{
			if(Integer.parseInt(s.substring(0, 1),16)>7)
			{
		    	for(int i=0;i<binString.length();i++)
		    	{
		    		if(binString.charAt(i)=='1')
		    		{
		    			sTemp=sTemp+'0';
		    		}
		    		else
		    		{
		    			sTemp=sTemp+'1';
		    		}
		    	}
		    	dLatitudeandLng=-(Integer.parseInt(sTemp,2)+1);
		    	
			}
			else
			{
				dLatitudeandLng=Integer.parseInt(binString,2);
			}
    	}
			catch(Exception e)
			{
				e.printStackTrace();
			}
    	//System.out.println(sTemp);
    	//int d=Integer.parseInt(sTemp,10);
    	//System.out.println(d);
    	return (dLatitudeandLng/1000000.0d);
    }
    public static String HexToBinary(String Hex) throws Exception{

        String sHex = new BigInteger(Hex.trim(), 16).toString(2).equals("0") ? "00000000" : new BigInteger(Hex.trim(), 16).toString(2);
        if(sHex.length()==1){
            return "0000000"+sHex;
        }
        else if(sHex.length()==2){
            return "000000"+sHex;
        }
        else if(sHex.length()==3){
            return "00000"+sHex;
        }
        else if(sHex.length()==4){
            return "0000"+sHex;
        }
        else if(sHex.length()==5){
            return "000"+sHex;
        }
        else if(sHex.length()==6){
            return "00"+sHex;
        }
        else if(sHex.length()==7){
            return "0"+sHex;
        }
        return sHex;
    }
}
