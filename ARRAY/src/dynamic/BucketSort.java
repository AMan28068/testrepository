package dynamic;

public class BucketSort {
	public static int max(int ar[])
	{
		int max=ar[0];
		for(int iter=1;iter<ar.length;iter++)
		{
			if(ar[iter]>max)
				max=ar[iter];
		}
		return max;
	}
	public static double divider(int max)
	{
		int x=(int)Math.ceil((max+1)/10.0);
		//System.out.println(x);
		return x;
	}
	public static void bucketsort(int ar[],int divider)
	{
		BINARYTREEE[] btree=new BINARYTREEE[10];
		int j=0;
		for(int iter=0;iter<ar.length;iter++)
		{
			j=(int)Math.floor(ar[iter]/divider);
			if(btree[j]==null)
			{
				btree[j]=new BINARYTREEE();
				btree[j].node=btree[j].createNode(ar[iter],btree[j].node);
			}
			else
			{
				btree[j].createNode(ar[iter], btree[j].node);
			}
		}
		int j1=0;
		for(int iter=0;iter<10;iter++)
		{
			if(btree[iter]!=null)
			{
				int ar1[]=btree[iter].nonThreadedInorder(btree[iter].node);
				for(int iter1=0;iter1<ar1.length;iter1++)
					ar[j1++]=ar1[iter1];
			}
		}
	}
	public static void main(String arp[])
	{
		int ar[]= {170, 45, 75, 90, 802, 24, 2, 66};
		int max=max(ar);
		int divider=(int)divider(max);
		bucketsort(ar,divider);
		for(int x:ar)
			System.out.println(x);
	}
}
