package dynamic;

public class Stack {
	Nodes head;
	public Stack()
	{
		head=null;
	}
	public Nodes InsertNode(Nodes root,int data)
	{
		Nodes x=new Nodes(data);
		if(root==null)
		{
			this.head=x;
			return x;
		}
		else
		{
			x.next=this.head;
			this.head=x;
			return x;
		}
	}
	public Nodes RemoveNode(Nodes root)
	{
		if(root==null)
			return null;
		if(root.next==null)
		{	this.head=null;
			return root;
		}
		else
		{
			Nodes x=root.next;
			this.head=x;
			root.next=null;
			return root; 
		}
	}
	
}
