package dynamic;

public class MAXProfit {
	public static int maxProfit(int ar[])
	{
		int profit[]=new int[ar.length];
		int maxPrice=ar[ar.length-1];
		int minPrice=ar[0];
		for(int i=0;i<ar.length;i++)
			profit[i]=0;
		for(int i=ar.length-2;i>=0;i--)
		{	if(maxPrice<ar[i])
				maxPrice=ar[i];
			profit[i]=Math.max(maxPrice-ar[i], profit[i+1]);
		}
		for(int i=1;i<ar.length;i++)
		{
			if(minPrice>ar[i])
				minPrice=ar[i];
			if(i==ar.length-1)
				break;
			profit[i]=Math.max(profit[i-1],(ar[i]-minPrice)+profit[i+1]);
		}
		return profit[ar.length-2];
	}
	public static void main(String ar[])
	{
		int arr[]= {2, 30, 15, 10, 8, 25, 80};
		System.out.println(maxProfit(arr));
	} 
}
