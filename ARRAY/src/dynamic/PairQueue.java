package dynamic;

public class PairQueue {
		PairQueue head;
		PairNode data;
		PairQueue next;
		public PairQueue(Node data,int horizontalDist)
		{
			this.data=new PairNode();
			this.data.data=data;
			this.data.horizontalDistance=horizontalDist;
			this.next=null;
		}
		public PairQueue insert(Node x,PairQueue x2,int horizontalDist)
		{
			PairQueue x1=new PairQueue(x,horizontalDist);
			if(x2==null)
			{
				
				return x1;
			}
			else
			{
				x1.next=x2;
				return x1;
			}
		}
		public PairNode Remove(PairQueue x)
		{
			if(x==null)
			{
				return null;
			}
			else
			{
				if(x.next==null || x.next.data==null)
				{
					PairNode data=x.data;
					
					x.data=null;
					x=null;
					return data;
					
				}
				else
				{
					PairNode temp=null;
					PairQueue temp1=null;
					while(x.next!=null && x.next.data!=null)
					{
						temp1=x;
						x=x.next;
					}
					temp=x.data;
					temp1.next=null;
					return temp;
				}
			}
		}
		public int size(PairQueue queue)
		{
			int c=0;
			if(queue==null)
			{
				return 0;
			}
			if(queue.next==null)
			{
				return 1;
			}
			while(queue!=null)
			{
				c++;
				queue=queue.next;
			}
			return c;
		}
	}


