package dynamic;

public class MaxJumpToReachEnd {
	public static int noJumps(int ar[])
	{
		if(ar[0]==0)
		{
			return -1;
		}
		int len=ar.length;
		//int maxReach=ar[0];
		int max=0;
		int currMax=ar[0];
		boolean maxAssigned=false;
		int j=0;
		for(int i=1;i<len;i++)
		{
				if(currMax==0)
				{
					return -1;
				}
				if(ar[i]>max && currMax>0)
				{
					max=ar[i];
					maxAssigned=true;
				}
				else if(maxAssigned && max>1)
				{
					max--;
				}
				currMax--;
				if(currMax<=0) //success
				{
					j++;
					currMax=max;
					max=0;
					maxAssigned=false;
					if(currMax==1 && ar[i]==0) //test the condition whenever the there is a preceding 1 and succeeding 0
					{
						return -1;
					}
				}
				
		}
		if(currMax>1) //when element is more than to reach last element
		{
			j++;
		}
		return j;
	}
	public static void main(String ar[])
	{
		int arr[]= {1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9};
		System.out.println(noJumps(arr));
		
	}
}
