package dynamic;

public class ArrayRotationJuggling {
	
	public static int GCD(int a,int b)
	{
		if(a==0 ||b==0)
		{
			return 0;
		}
		if(a==b)
		{
			return b;
			
		}
		if(a>b)
			return GCD(a-b,b);
		return GCD(a,b-a);
		
	}
	public static void ArrayRotation(int ar[],int r)
	{
		int j=0;
		int temp=0;
		int k=0;
		for(int i=0;i<GCD(ar.length,r);i++)
		{
			j=i;
			temp=ar[i];
			while(true)
			{
				k=(j+r)%ar.length;
				ar[j]=ar[k];
				if(k==i)
					break;
				j=k;
			}
			ar[j]=temp;
		}
		
	}
	public static void main(String ar[])
	{
		int arr[]= {1,2,3,4,5,6,7,8,9,10};
		
		ArrayRotation(arr, 3);
		for(int x:arr)
		{
			System.out.println(x);
		}
	}
}
