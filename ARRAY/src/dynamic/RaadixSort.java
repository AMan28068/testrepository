package dynamic;

import java.util.HashMap;

public class RaadixSort {
	public static int max(int ar[])
	{
		int max=ar[0];
		for(int i=1;i<ar.length;i++)
		{
			if(ar[i]>max)
				max=ar[i];
		}
		return max;
	}
	public static void countSort(int ar[],int exp)
	{
		//HashMap<Integer,Integer> hsh=new HashMap<Integer,Integer>();
		LinkedLis[] link=new LinkedLis[10];
		int ar1[]=new int[ar.length];
		for(int i=0;i<10;i++)
		{
			link[i]=null;
		}
		for(int i=0;i<ar.length;i++)
		{
			if(link[(ar[i]/exp)%10]==null)
			{
				link[(ar[i]/exp)%10]=new LinkedLis();
				link[(ar[i]/exp)%10].head=link[(ar[i]/exp)%10].insertNode(null, i);
			}
			else
			{
				link[(ar[i]/exp)%10].head=link[(ar[i]/exp)%10].insertNode(link[(ar[i]/exp)%10].head, i);
			}	
		}
		int j=0;
		for(int i=0;i<ar.length;)
		{
			if(link[j]!=null)
			{
				int size=link[j].size(link[j].head);
				while(size>0)
				{
					int index=link[j].RemoveNode(link[j].head);
					if(index!=-1)
						ar1[i++]=ar[index];
					size--;
				}
			}
			j++;
		}
		for(int i=0;i<ar.length;i++)
			ar[i]=ar1[i];
		
	}
	public static void main(String ar[])
	{
		int arr[]= {170, 45, 75, 90, 802, 24, 2, 66};
		System.out.println(max(arr));
		int max=max(arr);
		for(int i=1;max/i>0;i*=10)
			countSort(arr, i);
		for(int x: arr)
			System.out.println(x);
	}
}
