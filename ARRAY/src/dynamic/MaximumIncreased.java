package dynamic;

public class MaximumIncreased {
	public static int maximumIncrease(int ar[])
	{
		int maxSum[]=new int [ar.length];
		for(int i=0;i<ar.length;i++)
		{
			maxSum[i]=ar[i];
		}
		int max=maxSum[0];
		for(int i=1;i<ar.length;i++)
		{
			for(int j=0;j<ar.length;j++)
			{
				if(ar[i]>ar[j] && maxSum[i]<maxSum[j]+ar[i])
				{
					maxSum[i]=ar[i]+maxSum[j];	
					if(max<maxSum[i])
					{
						max=maxSum[i];
					}
				}
			}
		}
		return max;
	}
	public static void main(String arp[])
	{
		int ar[]= {1,8,2,4,11,6,7,8,9};
		System.out.println(maximumIncrease(ar));
	}
}
