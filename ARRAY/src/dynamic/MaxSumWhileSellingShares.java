package dynamic;

public class MaxSumWhileSellingShares {
	public static int maxBuyAndShare(int ar[])
	{
		int maxprice=ar[ar.length-1];
		int minprice=0;
		int profit[]=new int[ar.length];
		for(int i=0;i<ar.length;i++)
			profit[i]=0;
		for(int i=ar.length-2;i>=0;i--)
		{
			if(maxprice<ar[i])
				maxprice=ar[i];
			profit[i]=Math.max(profit[i+1], maxprice-ar[i]);
		}
		minprice=ar[0];
		for(int i=1;i<ar.length;i++)
		{
			if(minprice>ar[i])
				minprice=ar[i];
			int x=ar[i]-minprice;
			if(i==ar.length-1)
			{
				break;
			}
			profit[i]=Math.max(profit[i-1], profit[i+1]+(ar[i]-minprice));
		}
		return profit[ar.length-2];
	}
	public static void main(String ar[])
	{
		int ar1[]= {100, 30, 15, 10, 8,75,25, 80};
		System.out.println(maxBuyAndShare(ar1));
		

	}
}
