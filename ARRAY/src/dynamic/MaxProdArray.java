package dynamic;

public class MaxProdArray {
	public static void MaxSubarray(long ar[])
	{
		//boolean zeroReached=false;
		boolean positiveStream=false;
		long max=1;
		long maxbeforeZero=0;
		long maxProd=1;
		int negCount=0;
		int onePositive=0;
		for(int i=0;i<ar.length;i++)
		{
			if(ar[i]==0)
			{
				if(negCount>0)
					negCount--;
				continue;
			}
			else if(ar[i]>0)
				
			{
				onePositive++;
				break;
			}
			else 
			{
				negCount++;
			}
			
		}
		if(negCount==1 && onePositive==0) 
			System.out.println("Max is "+0);
		else {
			for(int i=0;i<ar.length;i++)
			{
				if(ar[i]>0)
				{
					if((positiveStream) || maxProd>0)
					{
						max=ar[i]*max;
						maxProd=ar[i]*maxProd;
					}
					else
					{
						max=Math.max(max, ar[i]);
						maxProd=maxProd*ar[i];
						
					}
					positiveStream=true;
				}
				
				else if(ar[i]==0)
				{
					
					if(maxbeforeZero==0)
						maxbeforeZero=max;
					else
					{
						if(maxbeforeZero<max)
							maxbeforeZero=max;
					}
					positiveStream=false;
					maxProd=1;
					max=1;
					
				}
				else
				{
					maxProd=maxProd*ar[i];
					if(maxProd>0 && maxProd>max)
						max=maxProd;
					positiveStream=false;
				}
				
			}
			if(maxbeforeZero<max)
				maxbeforeZero=max;
			System.out.println(maxbeforeZero);
		}
	}
	public static void main(String ar[])
	{
		long ar1[]= {-8,-8,-8,-8,0,-8};
		MaxSubarray(ar1);
	}
}
