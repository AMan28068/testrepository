package dynamic;

public class Queue {
	Queue head;
	Node data;
	Queue next;
	public Queue(Node data)
	{
		this.data=data;
		this.next=null;
	}
	public Queue insert(Node x,Queue x2)
	{
		Queue x1=new Queue(x);
		if(x2==null)
		{
			
			return x1;
		}
		else
		{
			x1.next=x2;
			return x1;
		}
	}
	public Node Remove(Queue x)
	{
		if(x==null)
		{
			return null;
		}
		else
		{
			if(x.next==null || x.next.data==null)
			{
				Node data=x.data;
				this.head=null;
				x.data=null;
				x=null;
				return data;
				
			}
			else
			{
				Node temp=null;
				Queue temp1=null;
				while(x.next!=null && x.next.data!=null)
				{
					temp1=x;
					x=x.next;
				}
				temp=x.data;
				temp1.next=null;
				return temp;
			}
		}
	}
	public int size(Queue queue)
	{
		int c=0;
		if(queue==null)
		{
			return 0;
		}
		if(queue.next==null)
		{
			return 1;
		}
		while(queue!=null)
		{
			c++;
			queue=queue.next;
		}
		return c;
	}
}
