package dynamic;

public class MaximuSumIncreasing {
	public static int maxSumIncreasing(int ar[])
	{
		int temp[]=new int[ar.length];
		for(int i=0;i<ar.length;i++)
		{
			temp[i]=ar[i];
		}
		for(int i=1;i<ar.length;i++)
		{
			for(int j=0;j<i;j++)
			{
				if(ar[i]>ar[j])
				{
					if(ar[i]+temp[j]>temp[i])
					{
						temp[i]=ar[i]+temp[j];
					}
				}
			}
		}
		int max=ar[0];
		for(int i=0;i<ar.length;i++)
		{
			if(temp[i]>max)
			{
				max=temp[i];
			}
		}
		return max;
	}
	public static void main(String ar[])
	{
		int arr[]= {1,8,2,4,11,6,7,8,9};
		System.out.println(maxSumIncreasing(arr));
		
	}
}
