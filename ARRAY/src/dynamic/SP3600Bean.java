package dynamic;

public class SP3600Bean {
	private long lImei=0;
	private String sDate="";
	private String sTime="";
	private long lFixtime=0l;
	private boolean bGPSValidity=true;
	private int iSatelites=0;
	private double dLatitude=0.0d;
	private double dLongitude=0.0d;
	private int iAzimuth=0;
	private int ispeed=0;
	private int iAltitude=0;
	private double dHDOP=0.0d;
	private double dVDOP=0.0d;
	private int iGSMSignalStrength=0;
	private int iMCC1=0;
	private int iMCC2=0;
	private int iMCC3=0;
	private int iMNC1=0;
	private int iMNC2=0;
	private int iMNC3=0;
	private int iLAC1=0;
	private int iLAC2=0;
	private int iLAC3=0;
	private int iCID1=0;
	private int iCID2=0;
	private int iCID3=0;
	private int iRSS1=0;
	private int iRSS2=0;
	private int iRSS3=0;
	private long lOdom=0l;
	private long lIN1=0;
	private long lIN2=0;
	private long lIN3=0;
	private long lIN4=0;
	private double dExtVol=0.0d;
	private double dBackVol=0.0d;
	private double dADC1Vol=0.0d;
	private double dADC2Vol=0.0d;
	private short shExtPwrsupp=0;
	private short shBackupPwrsupp=0;
	private short shMoving=0;
	private short shOverSpeed=0;
	private short shIdle=0;
	private short shTow=0;
	private short shADC1Status=0;
	private short shADC2Status=0;
	private short shAntiJam=0;
	private short shDomesticRoaming=0;
	private short shInterRoaming=0;
	private short shHarshBehavior=0;
	private short shAccident=0;
	private short shGeoFence=0;
	private short shParked=0;
	private short shWorkStatus=0;
	private short shAccOn=0;
	private short shDI1=0;
	private short shDI2=0;
	private short shDI3=0;
	private short shDI4=0;
	private short shDO1=0;
	private short shDO2=0;
	private short shD03=0;
	private short shFirstTimeReport=0;
	private double dTemp=0.0d;
	private int iEvent=-1;
	private String sCommand="";
	private int iOverSpeedStatus=0;
	private short shSocket1=0;
	private short shSocket2=0;
	private short shSocket3=0;
	private short shSocket4=0;
	private short shSocket5=0;
	private short shMotionSensor=0;
	private short shFlashStorage=0;
	private short shVibrationSensor=0;
	private short shHarshBrake=0;
	private short shHarshAccceleration=0;
	private short shHarshCornering=0;
	private short shFrontCollision=0;
	private short shRearCollision=0;
	private short shTurnOver=0;
	private short shTemperature=0;
	private short shIButton=0;
	private short shSleepMode=0;
	private short shBalanceLow=0;
	private double dXAxis=0.0d;
	private double dYAxis=0.0d;
	private int iGSM=0;
	private String sRfid="0";
	private double dLastLatitude = 0.0;
	private double dLastLongitude = 0.0;
	private int iCompanyId = 0;
	private int iVehicleId = 0;
	private int iResellerId = 0;
	private String sModelName = "";
	private boolean bOdom = true;
	public long getlImei() {
		return lImei;
	}
	public double getdLastLatitude() {
		return dLastLatitude;
	}
	public void setdLastLatitude(double dLastLatitude) {
		this.dLastLatitude = dLastLatitude;
	}
	public double getdLastLongitude() {
		return dLastLongitude;
	}
	public void setdLastLongitude(double dLastLongitude) {
		this.dLastLongitude = dLastLongitude;
	}
	public int getiCompanyId() {
		return iCompanyId;
	}
	public void setiCompanyId(int iCompanyId) {
		this.iCompanyId = iCompanyId;
	}
	public int getiVehicleId() {
		return iVehicleId;
	}
	public void setiVehicleId(int iVehicleId) {
		this.iVehicleId = iVehicleId;
	}
	public int getiResellerId() {
		return iResellerId;
	}
	public void setiResellerId(int iResellerId) {
		this.iResellerId = iResellerId;
	}
	public String getsModelName() {
		return sModelName;
	}
	public void setsModelName(String sModelName) {
		this.sModelName = sModelName;
	}
	public boolean isbOdom() {
		return bOdom;
	}
	public void setbOdom(boolean bOdom) {
		this.bOdom = bOdom;
	}
	public void setlImei(long lImei) {
		this.lImei = lImei;
	}
	public String getsDate() {
		return sDate;
	}
	public void setsDate(String sDate) {
		this.sDate = sDate;
	}
	public String getsTime() {
		return sTime;
	}
	public void setsTime(String sTime) {
		this.sTime = sTime;
	}
	public long getlFixtime() {
		return lFixtime;
	}
	public void setlFixtime(long lFixtime) {
		this.lFixtime = lFixtime;
	}
	public boolean isbGPSValidity() {
		return bGPSValidity;
	}
	public void setbGPSValidity(boolean bGPSValidity) {
		this.bGPSValidity = bGPSValidity;
	}
	public int getiSatelites() {
		return iSatelites;
	}
	public void setiSatelites(int iSatelites) {
		this.iSatelites = iSatelites;
	}
	public double getdLatitude() {
		return dLatitude;
	}
	public void setdLatitude(double dLatitude) {
		this.dLatitude = dLatitude;
	}
	public double getdLongitude() {
		return dLongitude;
	}
	public void setdLongitude(double dLongitude) {
		this.dLongitude = dLongitude;
	}
	public int getiAzimuth() {
		return iAzimuth;
	}
	public void setiAzimuth(int iAzimuth) {
		this.iAzimuth = iAzimuth;
	}
	public int getIspeed() {
		return ispeed;
	}
	public void setIspeed(int ispeed) {
		this.ispeed = ispeed;
	}
	public int getiAltitude() {
		return iAltitude;
	}
	public void setiAltitude(int iAltitude) {
		this.iAltitude = iAltitude;
	}
	public double getdHDOP() {
		return dHDOP;
	}
	public void setdHDOP(double dHDOP) {
		this.dHDOP = dHDOP;
	}
	public double getdVDOP() {
		return dVDOP;
	}
	public void setdVDOP(double dVDOP) {
		this.dVDOP = dVDOP;
	}
	public int getiGSMSignalStrength() {
		return iGSMSignalStrength;
	}
	public void setiGSMSignalStrength(int iGSMSignalStrength) {
		this.iGSMSignalStrength = iGSMSignalStrength;
	}
	public int getiMCC1() {
		return iMCC1;
	}
	public void setiMCC1(int iMCC1) {
		this.iMCC1 = iMCC1;
	}
	public int getiMCC2() {
		return iMCC2;
	}
	public void setiMCC2(int iMCC2) {
		this.iMCC2 = iMCC2;
	}
	public int getiMCC3() {
		return iMCC3;
	}
	public void setiMCC3(int iMCC3) {
		this.iMCC3 = iMCC3;
	}
	public int getiMNC1() {
		return iMNC1;
	}
	public void setiMNC1(int iMNC1) {
		this.iMNC1 = iMNC1;
	}
	public int getiMNC2() {
		return iMNC2;
	}
	public void setiMNC2(int iMNC2) {
		this.iMNC2 = iMNC2;
	}
	public int getiMNC3() {
		return iMNC3;
	}
	public void setiMNC3(int iMNC3) {
		this.iMNC3 = iMNC3;
	}
	public int getiLAC1() {
		return iLAC1;
	}
	public void setiLAC1(int iLAC1) {
		this.iLAC1 = iLAC1;
	}
	public int getiLAC2() {
		return iLAC2;
	}
	public void setiLAC2(int iLAC2) {
		this.iLAC2 = iLAC2;
	}
	public int getiLAC3() {
		return iLAC3;
	}
	public void setiLAC3(int iLAC3) {
		this.iLAC3 = iLAC3;
	}
	public int getiCID1() {
		return iCID1;
	}
	public void setiCID1(int iCID1) {
		this.iCID1 = iCID1;
	}
	public int getiCID2() {
		return iCID2;
	}
	public void setiCID2(int iCID2) {
		this.iCID2 = iCID2;
	}
	public int getiCID3() {
		return iCID3;
	}
	public void setiCID3(int iCID3) {
		this.iCID3 = iCID3;
	}
	public int getiRSS1() {
		return iRSS1;
	}
	public void setiRSS1(int iRSS1) {
		this.iRSS1 = iRSS1;
	}
	public int getiRSS2() {
		return iRSS2;
	}
	public void setiRSS2(int iRSS2) {
		this.iRSS2 = iRSS2;
	}
	public int getiRSS3() {
		return iRSS3;
	}
	public void setiRSS3(int iRSS3) {
		this.iRSS3 = iRSS3;
	}
	public long getlOdom() {
		return lOdom;
	}
	public void setlOdom(long lOdom) {
		this.lOdom = lOdom;
	}
	public long getlIN1() {
		return lIN1;
	}
	public void setlIN1(long lIN1) {
		this.lIN1 = lIN1;
	}
	public long getlIN2() {
		return lIN2;
	}
	public void setlIN2(long lIN2) {
		this.lIN2 = lIN2;
	}
	public long getlIN3() {
		return lIN3;
	}
	public void setlIN3(long lIN3) {
		this.lIN3 = lIN3;
	}
	public long getlIN4() {
		return lIN4;
	}
	public void setlIN4(long lIN4) {
		this.lIN4 = lIN4;
	}
	public double getdExtVol() {
		return dExtVol;
	}
	public void setdExtVol(double dExtVol) {
		this.dExtVol = dExtVol;
	}
	public double getdBackVol() {
		return dBackVol;
	}
	public void setdBackVol(double dBackVol) {
		this.dBackVol = dBackVol;
	}
	public double getdADC1Vol() {
		return dADC1Vol;
	}
	public void setdADC1Vol(double dADC1Vol) {
		this.dADC1Vol = dADC1Vol;
	}
	public double getdADC2Vol() {
		return dADC2Vol;
	}
	public void setdADC2Vol(double dADC2Vol) {
		this.dADC2Vol = dADC2Vol;
	}
	public short getShExtPwrsupp() {
		return shExtPwrsupp;
	}
	public void setShExtPwrsupp(short shExtPwrsupp) {
		this.shExtPwrsupp = shExtPwrsupp;
	}
	public short getShBackupPwrsupp() {
		return shBackupPwrsupp;
	}
	public void setShBackupPwrsupp(short shBackupPwrsupp) {
		this.shBackupPwrsupp = shBackupPwrsupp;
	}
	public short getShMoving() {
		return shMoving;
	}
	public void setShMoving(short shMoving) {
		this.shMoving = shMoving;
	}
	public short getShOverSpeed() {
		return shOverSpeed;
	}
	public void setShOverSpeed(short shOverSpeed) {
		this.shOverSpeed = shOverSpeed;
	}
	public short getShIdle() {
		return shIdle;
	}
	public void setShIdle(short shIdle) {
		this.shIdle = shIdle;
	}
	public short getShTow() {
		return shTow;
	}
	public void setShTow(short shTow) {
		this.shTow = shTow;
	}
	public short getShADC1Status() {
		return shADC1Status;
	}
	public void setShADC1Status(short shADC1Status) {
		this.shADC1Status = shADC1Status;
	}
	public short getShADC2Status() {
		return shADC2Status;
	}
	public void setShADC2Status(short shADC2Status) {
		this.shADC2Status = shADC2Status;
	}
	public short getShAntiJam() {
		return shAntiJam;
	}
	public void setShAntiJam(short shAntiJam) {
		this.shAntiJam = shAntiJam;
	}
	public short getShDomesticRoaming() {
		return shDomesticRoaming;
	}
	public void setShDomesticRoaming(short shDomesticRoaming) {
		this.shDomesticRoaming = shDomesticRoaming;
	}
	public short getShInterRoaming() {
		return shInterRoaming;
	}
	public void setShInterRoaming(short shInterRoaming) {
		this.shInterRoaming = shInterRoaming;
	}
	public short getShHarshBehavior() {
		return shHarshBehavior;
	}
	public void setShHarshBehavior(short shHarshBehavior) {
		this.shHarshBehavior = shHarshBehavior;
	}
	public short getShAccident() {
		return shAccident;
	}
	public void setShAccident(short shAccident) {
		this.shAccident = shAccident;
	}
	public short getShGeoFence() {
		return shGeoFence;
	}
	public void setShGeoFence(short shGeoFence) {
		this.shGeoFence = shGeoFence;
	}
	public short getShParked() {
		return shParked;
	}
	public void setShParked(short shParked) {
		this.shParked = shParked;
	}
	public short getShWorkStatus() {
		return shWorkStatus;
	}
	public void setShWorkStatus(short shWorkStatus) {
		this.shWorkStatus = shWorkStatus;
	}
	public short getShAccOn() {
		return shAccOn;
	}
	public void setShAccOn(short shAccOn) {
		this.shAccOn = shAccOn;
	}
	public short getShDI1() {
		return shDI1;
	}
	public void setShDI1(short shDI1) {
		this.shDI1 = shDI1;
	}
	public short getShDI2() {
		return shDI2;
	}
	public void setShDI2(short shDI2) {
		this.shDI2 = shDI2;
	}
	public short getShDI3() {
		return shDI3;
	}
	public void setShDI3(short shDI3) {
		this.shDI3 = shDI3;
	}
	public short getShDI4() {
		return shDI4;
	}
	public void setShDI4(short shDI4) {
		this.shDI4 = shDI4;
	}
	public short getShDO1() {
		return shDO1;
	}
	public void setShDO1(short shDO1) {
		this.shDO1 = shDO1;
	}
	public short getShDO2() {
		return shDO2;
	}
	public void setShDO2(short shDO2) {
		this.shDO2 = shDO2;
	}
	public short getShD03() {
		return shD03;
	}
	public void setShD03(short shD03) {
		this.shD03 = shD03;
	}
	public short getShFirstTimeReport() {
		return shFirstTimeReport;
	}
	public void setShFirstTimeReport(short shFirstTimeReport) {
		this.shFirstTimeReport = shFirstTimeReport;
	}
	public double getdTemp() {
		return dTemp;
	}
	public void setdTemp(double dTemp) {
		this.dTemp = dTemp;
	}
	public int getiEvent() {
		return iEvent;
	}
	public void setiEvent(int iEvent) {
		this.iEvent = iEvent;
	}
	public String getsCommand() {
		return sCommand;
	}
	public void setsCommand(String sCommand) {
		this.sCommand = sCommand;
	}
	public int getiOverSpeedStatus() {
		return iOverSpeedStatus;
	}
	public void setiOverSpeedStatus(int iOverSpeedStatus) {
		this.iOverSpeedStatus = iOverSpeedStatus;
	}
	public short getShSocket1() {
		return shSocket1;
	}
	public void setShSocket1(short shSocket1) {
		this.shSocket1 = shSocket1;
	}
	public short getShSocket2() {
		return shSocket2;
	}
	public void setShSocket2(short shSocket2) {
		this.shSocket2 = shSocket2;
	}
	public short getShSocket3() {
		return shSocket3;
	}
	public void setShSocket3(short shSocket3) {
		this.shSocket3 = shSocket3;
	}
	public short getShSocket4() {
		return shSocket4;
	}
	public void setShSocket4(short shSocket4) {
		this.shSocket4 = shSocket4;
	}
	public short getShSocket5() {
		return shSocket5;
	}
	public void setShSocket5(short shSocket5) {
		this.shSocket5 = shSocket5;
	}
	public short getShMotionSensor() {
		return shMotionSensor;
	}
	public void setShMotionSensor(short shMotionSensor) {
		this.shMotionSensor = shMotionSensor;
	}
	public short getShFlashStorage() {
		return shFlashStorage;
	}
	public void setShFlashStorage(short shFlashStorage) {
		this.shFlashStorage = shFlashStorage;
	}
	public short getShVibrationSensor() {
		return shVibrationSensor;
	}
	public void setShVibrationSensor(short shVibrationSensor) {
		this.shVibrationSensor = shVibrationSensor;
	}
	public short getShHarshBrake() {
		return shHarshBrake;
	}
	public void setShHarshBrake(short shHarshBrake) {
		this.shHarshBrake = shHarshBrake;
	}
	public short getShHarshAccceleration() {
		return shHarshAccceleration;
	}
	public void setShHarshAccceleration(short shHarshAccceleration) {
		this.shHarshAccceleration = shHarshAccceleration;
	}
	public short getShHarshCornering() {
		return shHarshCornering;
	}
	public void setShHarshCornering(short shHarshCornering) {
		this.shHarshCornering = shHarshCornering;
	}
	public short getShFrontCollision() {
		return shFrontCollision;
	}
	public void setShFrontCollision(short shFrontCollision) {
		this.shFrontCollision = shFrontCollision;
	}
	public short getShRearCollision() {
		return shRearCollision;
	}
	public void setShRearCollision(short shRearCollision) {
		this.shRearCollision = shRearCollision;
	}
	public short getShTurnOver() {
		return shTurnOver;
	}
	public void setShTurnOver(short shTurnOver) {
		this.shTurnOver = shTurnOver;
	}
	public short getShTemperature() {
		return shTemperature;
	}
	public void setShTemperature(short shTemperature) {
		this.shTemperature = shTemperature;
	}
	public short getShIButton() {
		return shIButton;
	}
	public void setShIButton(short shIButton) {
		this.shIButton = shIButton;
	}
	public short getShSleepMode() {
		return shSleepMode;
	}
	public void setShSleepMode(short shSleepMode) {
		this.shSleepMode = shSleepMode;
	}
	public short getShBalanceLow() {
		return shBalanceLow;
	}
	public void setShBalanceLow(short shBalanceLow) {
		this.shBalanceLow = shBalanceLow;
	}
	public double getdXAxis() {
		return dXAxis;
	}
	public void setdXAxis(double dXAxis) {
		this.dXAxis = dXAxis;
	}
	public double getdYAxis() {
		return dYAxis;
	}
	public void setdYAxis(double dYAxis) {
		this.dYAxis = dYAxis;
	}
	public int getiGSM() {
		return iGSM;
	}
	public void setiGSM(int iGSM) {
		this.iGSM = iGSM;
	}
	public String getsRfid() {
		return sRfid;
	}
	public void setsRfid(String sRfid) {
		this.sRfid = sRfid;
	}

}
