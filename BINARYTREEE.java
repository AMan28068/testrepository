package dynamic;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class BINARYTREEE {
	Node node=null;
	public static int preIndex=0;
	public int size=0;
	public BINARYTREEE()
	{
	}
	public void Levelorder(Node x)
	{
		PairQueue queue=new PairQueue(x,0);
		queue.head=queue;
		//queue=queue.insert(x, queue);
		Map<Integer,Integer> VerticalContainer=new HashMap<Integer,Integer>();
		int hd=0;
		int count=queue.size(queue.head);
		
		while(count!=0)
		{
			
			PairNode data1=queue.Remove(queue.head);
			Node data=data1.data;
			int hddit=data1.horizontalDistance;
			if(VerticalContainer.containsKey(hddit))
			{
				//LinkedLis link=VerticalContainer.get(hddit);			//Print the vertical Tree 
				//link.head=link.insertNode(link.head, data.data);		//Print the vertical Tree
				int intdata=VerticalContainer.get(hddit);
				VerticalContainer.put(hddit, intdata+data.data);
			}
			else{
				//LinkedLis link=new LinkedLis();						//Print the vertical tree
				//link.head=link.insertNode(null,data.data);			//Print the vertical tree
				VerticalContainer.put(hddit,data.data);
			}
			if(queue.head.data==null)
				queue.head=null;
			System.out.print(data.data);
			
			
			if(data.left!=null)
				queue.head=queue.insert(data.left, queue.head,hddit+1);
			if(data.right!=null)
				queue.head=queue.insert(data.right, queue.head,hddit-1);
			count=queue.size(queue.head);
			
		}
		System.out.println("VERTICAL");
		/*for(Map.Entry<Integer, LinkedLis> entry:VerticalContainer.entrySet())			//Print the vertical tree
		{
			while(entry.getValue().size(entry.getValue().head)!=0)
				System.out.print(entry.getValue().RemoveNode(entry.getValue().head)+" , ");	//Print the vertical tree
			System.out.println();
		}*/
		for(Map.Entry<Integer, Integer> entry:VerticalContainer.entrySet())
		{
			System.out.print(entry.getValue());
			System.out.println();
		}
	}
	
	public Node createNode(int x,Node y)
	{
		
		if(y==null)
		{
			Node tempNode=new Node();
			tempNode.data=x;
			tempNode.left=null;
			tempNode.right=null;
			this.size++;
			return tempNode;
			
		}
		else
		{
			if(y.data>=x)
			{
				 y.left=createNode(x,y.left);
				 y.left.parent=y;
			}
			else
			{
				 y.right=createNode(x, y.right);
				 y.right.parent=y;
			}
		}
		return y;
	}
	public void Inorder(Node x)
	{
		if(x==null)
		{
			return;
		}
		Inorder(x.left);
		System.out.println(x.data);
		Inorder(x.right);
	}
	public void MorrisInorder(Node x)
	{
		Node root=x;
		Node current=x;
		while(root!=null)
		{
		if(root.left==null)
		{
			System.out.println(root.data);
			root=root.right;
		}
		else
		{
			current=root.left;
			while(current.right!=null && current.right!=root)
			{
				current=current.right;
			}
			if(current.right==root)
			{
				
				current.right=null;
				root=root.right;
			}
			else
			{
				System.out.println(root.data);
				current.right=root;
				root=root.left;
			}
		}
	}
	}
	public  void MorrisPreorder(Node x)
	{
		Node root=x;
		Node current=null;
		while(root!=null)
		{
			if(root.left==null)
			{
				System.out.println(root.data);
				root=root.right;
			}
			else
			{
				current=root.left;
				while(current.right!=null && current.right!=root)
					current=current.right;
				if(current.right==root)
				{
					System.out.println(root.data);
					//current=current.right;
					root=root.right;
					current.right=null;
				}	
				else
				{
					current.right=root;
					root=root.left;
				}
			}
		}
	}
	public void diagonalTree(Node x)
	{
		Node current=x;
		Queue queue=new Queue(x);
		queue.head=queue;
		
		
		while(queue.size(queue.head)!=0)
		{
			while(current.right!=null)
			{
				current=current.right;
				queue.head=queue.insert(current, queue.head);
			}
			Node temp=queue.Remove(queue.head);
			if(temp==null || temp.data==0)
				break;
				
			System.out.println(temp.data);
			if(temp.left!=null)
			{	current=temp.left;
				queue.head=queue.insert(current, queue.head);
		
			}
		}
		
	}
	public int[] nonThreadedInorder(Node r)
	{
		
		Node root=r;
		int ar[]=new int[this.size];
		int i=0;
		boolean bLeftDone=false;
		while(root!=null)
		{
			if(!bLeftDone)
			{
				while(root.left!=null)
					root=root.left;
			}
			if(root.left==null && root.right==null)
				
			//System.out.println(root.data);
			bLeftDone=true;
			if(root.right!=null)
			{
				root=root.right;
				bLeftDone=false;
			}
			else if(root.parent!=null)
			{
				while(root.parent!=null && root==root.parent.right)
					root=root.parent;
				if(root.parent==null)
					break;
				root=root.parent;
			}
			else 
				break;
			
		}
		return ar;
		
	}
	public void NonThreaded1(Node r,Node r1)
	{
		Node root=r;
		Node root1=r1;
		boolean bleftDone=false;
		boolean bleftDone1=false;
		boolean bTree1LeafFound=false;
		boolean bTree2LeafFound=false;
		int leaf1=0;
		int leaf2=0;
		boolean isSame=false;
		while(root!=null && root1!=null)
		{
			if(!bleftDone)
				while(root.left!=null)
					root=root.left;
			if(!bleftDone1)
				while(root1.left!=null)
					root1=root1.left;
			bleftDone=true;
			bleftDone1=true;
			if(root.left==null && root.right==null)
			{	
				bTree1LeafFound=true;
				leaf1=root.data;
			}
			if(root1.left==null && root1.right==null)
			{
				bTree2LeafFound=true;
				leaf2=root1.data;
			}
			if(bTree1LeafFound && bTree2LeafFound)
			{
				if(leaf1==leaf2)
				{
					isSame=true;
				}
				else
				{
					break;
				}
				leaf1=leaf2=0;
				bTree1LeafFound=bTree2LeafFound=false;
			}
			System.out.println(root.data);
			if(root.right!=null)
			{
				root=root.right;
				bleftDone=false;
			}
			else if(root.parent!=null)
			{
				while(root.parent!=null && root.parent.right==root)
					root=root.parent;
				if(root.parent==null)
					break;
				root=root.parent;
			}
			else
			{
				break;
			}
			if(root1.right!=null)
			{
				root1=root1.right;
				bleftDone1=false;
			}
			else if(root1.parent!=null)
			{
				while(root1.parent!=null && root1.parent.right==root1)
					root1=root1.parent;
				if(root1.parent==null)
					break;
				root1=root1.parent;
			}
			else
			{
				break;
			}
		}
		if((bTree1LeafFound==false && bTree2LeafFound==true) || (bTree1LeafFound==true && bTree2LeafFound==false))
			isSame=false;
		System.out.println(isSame);
	}
	public static void PrintLeftBoundaryNode(Node x)
	{
		if(x!=null)
		{
			if(x.left!=null)
			{
				PrintLeftBoundaryNode(x.left);
				System.out.println(x.data);
				
			}
			else if(x.right!=null)
			{
				PrintLeftBoundaryNode(x.right);
				System.out.println(x.data);
				
			}
		}
	}
	public static void PrintLeaves(Node x)
	{
		if(x!=null)
		{
			PrintLeaves(x.right);
			if(x.left==null && x.right==null)
				System.out.println(x.data);
			
			PrintLeaves(x.left);
		}
		
	}
	public static void PrintRightBoundary(Node x)
	{
		if(x!=null)
		{
			if(x.right!=null)
			{
				
				System.out.println(x.data);
				PrintRightBoundary(x.right);
			}
			else if(x.left!=null)
			{
				
				System.out.println(x.data);
				PrintRightBoundary(x.left);
			}
		}
	}
	public static void PrintTheBoundaryResult(Node x)
	{
		if(x!=null)
		{	
			System.out.println(x.data);
			
			
			PrintRightBoundary(x.right);
			PrintLeaves(x);
			PrintLeftBoundaryNode(x.left);
		}
		
	}
	public int  SearchTree(int ar[],int start,int end,int data)
	{
		int i;
		for(i=start;i<=end;i++)
		{
			if(ar[i]==data)
				return i;
		}
		return i;
	}
	public Node BuildTreeFromInorderAndPreorder(int pre[],int inorder[],int startIndex,int endIndex)
	{
		if(startIndex>endIndex)
			return null;
		Node node=new Node();
		node.data=pre[preIndex++];
		System.out.println(node.data+" end index "+endIndex+"  start index "+startIndex);
		if(startIndex==endIndex)
			return node;
		int searchIndex=SearchTree(inorder,startIndex,endIndex,node.data);
		System.out.println(searchIndex);
		node.left=BuildTreeFromInorderAndPreorder(pre, inorder, startIndex, searchIndex-1);
		node.right=BuildTreeFromInorderAndPreorder(pre, inorder, searchIndex+1, endIndex);
		return node;
	}
	static int post=0;
	public Node BuildTreeFromInorderAndPostOrder(int pre[],int inorder[],int startIndex,int endIndex)
	{
		if(startIndex>endIndex)
			return null;
		Node node=new Node();
		node.data=pre[post--];
		if(startIndex==endIndex)
			return node;
		int searchIndex=SearchTree(inorder, startIndex, endIndex, node.data);
		node.right=BuildTreeFromInorderAndPostOrder(pre, inorder,searchIndex+1 , endIndex);
		node.left=BuildTreeFromInorderAndPostOrder(pre, inorder, startIndex,searchIndex-1 );
		return node;
	}
	public void PerfectBinaryTreeSpecificLevelOrder(Node r)
	{
		Node root=r;
		System.out.println(root.data);
		Queue qRight=new Queue(root.right);
		qRight.head=qRight;
		Queue qLeft=new Queue(root.left);
		qLeft.head=qLeft;
		while(qRight.size(qRight.head)!=0 && qLeft.size(qLeft.head)!=0)
		{
			Node leftData=qLeft.Remove(qLeft.head);
			System.out.println(leftData.data);
			Node rightData=qRight.Remove(qRight.head);
			System.out.println(rightData.data);
			if(leftData.left!=null)
				qLeft.head=qLeft.insert(leftData.left,qLeft.head);
			if(leftData.right!=null)
				qLeft.head=qLeft.insert(leftData.right,qLeft.head);
			if(rightData.right!=null)
				qRight.head=qRight.insert(rightData.right,qRight.head);
			if(rightData.left!=null)
				qRight.head=qRight.insert(rightData.left, qRight.head);
			
		}
	}
	public static boolean root=true;
	public  Node buildUtil(int ar[],int i)
	{
		if(i>=ar.length || i>=ar.length)
			return null;
		Node root=new Node();
		root.data=ar[i];
		root.left=buildUtil(ar, 2*i+1);
		root.right=buildUtil(ar, 2*i+2);
		return root;
		
	}
	public  Node buildTreeFromLinkedList(LinkedLis list)
	{
		int list1[]=new int[list.size(list.head)];
		for(int i=0;i<list1.length;i++)
			list1[i]=list.removeFromBegin(list.head);
		System.out.println("BINARY ARRAY");
		for(int i=0;i<list1.length;i++)
			System.out.println(list1[i]);
		Node par=buildUtil(list1,0);
		return par;
		
	}
	public  boolean rootDone=true;
	public  Node BuildUtil12(int in[],int lvl[],int startIndex,int endIndex,int lvlOrderIndex)
	{
		if(startIndex>endIndex)
			return null;
		if(startIndex==endIndex)
		{
			Node x=new Node();
			x.data=in[startIndex];
			return x;
		}
			
		if(rootDone)
		{
			Node x=new Node();
			x.data=lvl[lvlOrderIndex];
			int searchIndex=SearchTree(in, startIndex, endIndex, x.data);
			rootDone=false;
			x.left=BuildUtil12(in, lvl, startIndex, searchIndex-1,lvlOrderIndex+1);
			x.right=BuildUtil12(in, lvl, searchIndex+1, endIndex,lvlOrderIndex+1);
			return x;
		}
		else
		{
			int data=0;
			boolean found=false;
			for(int j=lvlOrderIndex;j<lvl.length;j++)
			{	
				for(int i =startIndex;i<=endIndex;i++)
				{
					if(lvl[j]==in[i])
					{
						data=lvl[j];
						found=true;
						break;
					}
					
				}
				if(found)
					break;
			}
			Node xy=new Node();
			xy.data=data;
			int searchIndex=SearchTree(in, startIndex, endIndex,xy.data);
			xy.left=BuildUtil12(in, lvl, startIndex, searchIndex-1,lvlOrderIndex+1);
			xy.right=BuildUtil12(in, lvl, searchIndex+1, endIndex,lvlOrderIndex+1);
			return xy;
		}
	}
	public  Node BuildTreeFromInorderAndLevelOrder(int in[],int lvl[])
	{
		Node temp=new Node();
		temp=BuildUtil12(in, lvl,0, in.length-1,0);
		return temp;
	}
	static int preorder=0;
	static int spacialCounter=0;
	public Node BuildSpacialTree(int pre[],char spac[])
	{
		if(spacialCounter==pre.length)
			return null;
		if(spac[spacialCounter++]=='L')
		{
			Node X=new Node();
			X.data=pre[preorder++];
			return X;
		}
		else
		{
			Node X=new Node();
			X.data=pre[preorder++];
			X.left=BuildSpacialTree(pre, spac);
			X.right=BuildSpacialTree(pre, spac);
			return X;
		}
	}
	static int preIndex21=0;
	public Node ConstructTree(int pre[],int post[],int startIndex,int endIndex)
	{
		if(preIndex21>=pre.length)
			return null;
		Node x=new Node();
		x.data=pre[preIndex21++];
		if(startIndex==endIndex || preIndex21>=pre.length)
			return x;
		int searchIndex=SearchTree(post, startIndex, endIndex, pre[preIndex21]);
		if(searchIndex<=endIndex)
		{
			x.left=ConstructTree(pre, post, startIndex, searchIndex);
			x.right=ConstructTree(pre, post, searchIndex+1, endIndex);
		}
			return x;
		
	}
	static boolean rootDone45=true;
	static int k=0;
	static int ar[]=null;
	static Stack stk;
	public void buildAncestorMatix(int arr[][],Node root)
	{
		if(root==null)
			return;
		if(stk==null)
			stk=new Stack();
		Nodes x=stk.head;
		if(x!=null)
		{
			while(x!=null)
			{
				arr[x.data][root.data]=1;
				x=x.next;
			}
		}
		stk.InsertNode(stk.head, root.data);
		buildAncestorMatix(arr, root.left);
		if(root.left!=null)
			stk.RemoveNode(stk.head);
		buildAncestorMatix(arr, root.right);
		if(root.right!=null)
			stk.RemoveNode(stk.head);
	}
	public Node BuildUtilTreefromAncestor(int mat[][])
	{
		LinkedLis[] link=new LinkedLis[mat[0].length];
		HashMap<Integer,Integer> hshmp=new HashMap<Integer,Integer>();
		int count=0;
		for(int i=0;i<mat[0].length;i++)
		{	for(int j=0;j<mat[0].length;j++)
			{
				if(mat[i][j]==1)
				{
					count++;
					if(link[i]==null)
						link[i]=new LinkedLis();
					link[i].head=link[i].insertNode(link[i].head, j);
					if(hshmp.containsKey(j))
						hshmp.put(j, hshmp.get(j)+1);
					else
						hshmp.put(j,1);
				}
			}
			if(count==mat[0].length-1)
				hshmp.put(i, 0);
			count=0;
		}
		HashMap<Integer,LinkedLis> freq=new HashMap<Integer,LinkedLis>();
		for(Map.Entry<Integer,Integer> abc: hshmp.entrySet())
		{
			if(freq.containsKey(abc.getValue()))
			{
				LinkedLis lis=freq.get(abc.getValue());
				lis.head=lis.insertNode(lis.head, abc.getKey());
				freq.put(abc.getValue(), lis);
			}
			else
			{
				LinkedLis lis=new LinkedLis();
				lis.head=lis.insertNode(lis.head, abc.getKey());
				freq.put(abc.getValue(),lis);
			}
		}
		System.out.println(freq);
		Node head=BuildTreefromAncestor(link,0,freq,0);
		return head;
	}
	public static boolean rottdone=false;
	public Node BuildTreefromAncestor(LinkedLis lis[],int level,HashMap<Integer,LinkedLis> freq,int data)
	{
		if(!freq.containsKey(level))
			return null;
		if(!rottdone)
		{
			Node root=new Node();
			root.data=freq.get(level).RemoveNode(freq.get(level).head);
			rottdone=true;
			root.left=BuildTreefromAncestor(lis, level+1, freq,root.data);
			root.right=BuildTreefromAncestor(lis, level+1, freq,root.data);
			
			return root;
		}
		//int dat1=freq.get(level).removeFromBegin(freq.get(level).head);
		LinkedLis list=freq.get(level);
		Nodes temp=list.head;
		while(temp!=null)
		{
			if(lis[data]!=null)
			{
				boolean found=lis[data].FindSortedNode(lis[data].head,temp.data);
				if(found)
				{
					Node x=new Node();
					x.data=temp.data;
					list.FindSortedNode(list.head, temp.data);
					x.left=BuildTreefromAncestor(lis,level+1,freq,temp.data);
					x.right=BuildTreefromAncestor(lis, level+1, freq, temp.data);
					return x;
				}
			}
			else
			{
				return null;
			}
			temp=temp.next;
		}
		return null;
	}
	public Node ConstructCartesianTree(int ar[],int start,int end)
	{
		if(start>end)
			return null;
		Node node=new Node();
		int n=max(ar,start,end);
		node.data=ar[n];
		node.left=ConstructCartesianTree(ar, start, n-1);
		node.right=ConstructCartesianTree(ar, n+1, end);
		return node;
	}
	public int max(int ar[],int start,int end)
	{
		int max=ar[start];
		int maxInd=start;
		for(int i=start+1;i<=end;i++)
		{
			if(max<ar[i])
				maxInd=i;
		}
		return maxInd;
	}
	public Node buildTreeFromArrayUtil(HashMap<Integer,LinkedLis> hsh,int parentData)
	{
		if(!hsh.containsKey(parentData))
			return null;
		if(hsh.get(parentData).head==null)
			return null;
		int data=hsh.get(parentData).RemoveNode(hsh.get(parentData).head);
		Node node=new Node();
		node.data=data;
		node.left=buildTreeFromArrayUtil(hsh, data);
		node.right=buildTreeFromArrayUtil(hsh, data);
		return node;
	}
	public Node buildTreeFromArray(int ar[])
	{
		HashMap<Integer,LinkedLis> hsh=new HashMap<Integer,LinkedLis>();
		for(int i=0;i<ar.length;i++)
		{
			if(hsh.containsKey(ar[i]))
			{
				LinkedLis list=hsh.get(ar[i]);
				list.head=list.insertNode(list.head, i);
				hsh.put(ar[i],list);
			}
			else
			{
				LinkedLis list=new LinkedLis();
				list.head=list.insertNode(list.head, i);
				hsh.put(ar[i],list);
			}
		}
		Node node=buildTreeFromArrayUtil(hsh, -1);
		return node;
	}
	public static void main(String a[])
	{
		/*BINARYTREEE t=new BINARYTREEE();
		t.node=t.createNode(10,t.node);
		t.createNode(111,t.node);
		t.createNode(6,t.node);
		t.createNode(13,t.node);
		t.createNode(11,t.node);
		t.createNode(12,t.node);
		BINARYTREEE t1=new BINARYTREEE();
		t1.node=t1.createNode(10,t1.node);
		t1.createNode(111,t1.node);
		t1.createNode(7,t1.node);
		t1.createNode(13,t1.node);
		t1.createNode(11,t1.node);
		t1.createNode(12,t1.node);
		System.out.println("Level Order");
		//t.Inorder(t.node);
		t.Levelorder(t.node);
		System.out.println("Inorder is");
		t.NonThreaded1(t.node,t1.node);
		System.out.println("DIAGONAL TREE");
		t.diagonalTree(t.node);
		System.out.println(t);
		System.out.println("BOUNDARY TRAVERSAL");
		PrintTheBoundaryResult(t.node);
		BINARYTREEE t2=new BINARYTREEE();
		t2.node=t2.createNode(10,t2.node);
		t2.createNode(5,t2.node);
		t2.createNode(3,t2.node);
		t2.createNode(1, t2.node);	
		t2.createNode(2, t2.node);
		t2.createNode(7,t2.node);
		t2.createNode(6, t2.node);
		t2.createNode(8, t2.node);
		t2.createNode(15,t2.node);
		t2.createNode(12,t2.node);
		t2.createNode(11,t2.node);
		t2.createNode(13,t2.node);
		t2.createNode(18,t2.node);
		t2.createNode(16, t2.node);
		t2.createNode(20,t2.node);
		System.out.println("Level Order");
		t2.PerfectBinaryTreeSpecificLevelOrder(t2.node);
		int in[]= {10,30,40,50,60,70,90};
		int pre[]= {50,30,10,40,70,60,90};
		BINARYTREEE t3=new BINARYTREEE();
		System.out.println("INORDER JFNeo");
		t3.node=t3.BuildTreeFromInorderAndPreorder(pre, in, 0, pre.length-1);
		t3.Inorder(t3.node);
		BINARYTREEE t4=new BINARYTREEE();
		int post[]= {10,40,30,60,90,70,50};
		BINARYTREEE.post=post.length-1;
		t4.node=t4.BuildTreeFromInorderAndPostOrder(post, in, 0, post.length-1);
		System.out.println(":Inorder of treee:");
		t4.Inorder(t4.node);
		LinkedLis list=new LinkedLis();
		list.head=list.insertNode(list.head, 36);
		list.head=list.insertNode(list.head, 30);
		list.head=list.insertNode(list.head, 25);
		list.head=list.insertNode(list.head, 15);
		list.head=list.insertNode(list.head, 12);
		list.head=list.insertNode(list.head, 10);
		BINARYTREEE btree=new BINARYTREEE();
		btree.node=btree.buildTreeFromLinkedList(list);
		System.out.println("Inorder");
		btree.Inorder(btree.node);
		BINARYTREEE btre=new BINARYTREEE();
		int in1[]    = {4, 8, 10, 12, 14, 20, 22};
		int level[] = {20, 8, 22, 4, 12, 10, 14};
		
		btre.node=btre.BuildTreeFromInorderAndLevelOrder(in1, level);
		System.out.println("InordeXC");
		btre.Inorder(btre.node);
		System.out.println("Spacial Tree");
		BINARYTREEE bt=new BINARYTREEE();
		int pre1[]= {10, 30, 20, 5, 15,30,35};
		char spac[]= {'N', 'N', 'L', 'L', 'N','L','L'};
		bt.node=bt.BuildSpacialTree(pre1, spac);
		bt.Inorder(bt.node);
		System.out.println("BUILD IGNIFNOIS");
		int pre2[] = {1, 2, 4, 8, 9, 5, 3, 6, 7 };
        int post1[] = { 8, 9, 4, 5, 2, 6, 7, 3, 1 };
		BINARYTREEE btg=new BINARYTREEE();
		btg.node=btg.ConstructTree(pre2, post1, 0, pre2.length);
		System.out.println("Inorder frgr");
		btg.Inorder(btg.node);
		BINARYTREEE bttt=new BINARYTREEE();
		bttt.node=bttt.createNode(3, bttt.node);
		bttt.createNode(1,bttt.node );
		bttt.createNode(0,bttt.node );
		bttt.createNode(2, bttt.node);
		bttt.createNode(4,bttt.node);
		bttt.createNode(5, bttt.node);
		bttt.createNode(6, bttt.node);
		System.out.println("Size is"+bttt.size);
		int arr[][]=new int[bttt.size][bttt.size];
		ar=new int[bttt.size];
		int k=0;
		bttt.buildAncestorMatix(arr, bttt.node);
		for(int i=0;i<arr[0].length;i++)
		{
			for(int j=0;j<arr[0].length;j++)
			{
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}*/
		int matrx[][]= {{ 0, 0, 0, 0, 0, 0,0 },
		        { 1, 0, 0, 0, 1, 0,0 },
		        { 0, 0, 0, 1, 0, 0,1 },
		        { 0, 0, 0, 0, 0, 0,0 },
		        { 0, 0, 0, 0, 0, 0,0 },
		        { 1, 1, 1, 1, 1, 0,1 },
		        {0,0,0,0,0,0,0}
		    };
		System.out.println("BINARYTREEEE");
		BINARYTREEE bvc=new BINARYTREEE();
		bvc.node=bvc.BuildUtilTreefromAncestor(matrx);
		bvc.Inorder(bvc.node);
		/*System.out.println("Binartuahofnieg");
		int inorder[] = {5, 10, 40, 30, 28};
		BINARYTREEE btg1=new BINARYTREEE();
		btg1.node=btg1.ConstructCartesianTree(inorder, 0,inorder.length-1);
		btg1.Inorder(btg1.node);*/
		System.out.println("fkemgp");
		BINARYTREEE btg2=new BINARYTREEE();
		int parent[] = {1, 5, 5, 2, 2, -1, 3};
		btg2.node=btg2.buildTreeFromArray(parent);
		btg2.Inorder(btg2.node);
		
		/*
		t.MorrisInorder(t.node);
		System.out.println("Preoder is the..");
		t.MorrisPreorder(t.node);*/
		
	}
	
}
